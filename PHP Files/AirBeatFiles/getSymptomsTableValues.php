<?php

	require_once 'db_connection.php';

	
	$symptom1=0;
	$symptom2= 0;
	$symptom3=0;
	$symptom4=0;
	$symptom5=0;
	
	$date_current = $_POST['date'];
	$user_email = $_POST['email'];
	$overall_result = array();
	
	
	//----------------- Gtting the symptom IDs based on the user -----------------
	
	$sqlSymptom1Count = "SELECT COUNT(*) as count FROM usersymptoms US 
						INNER JOIN userinfo UI ON US.UserID=UI.UserID AND UI.Email='{$user_email}'
						WHERE US.CurrentDate='{$date_current}' AND US.SymptomID=1;";
	
	$responseSymptom1Count = mysqli_query($connection,$sqlSymptom1Count);
	
	while($row = mysqli_fetch_array($responseSymptom1Count)){
		$symptom1 = $row["count"];
	}
	
	$sqlSymptom2Count = "SELECT COUNT(*) as count FROM usersymptoms US 
						INNER JOIN userinfo UI ON US.UserID=UI.UserID AND UI.Email='{$user_email}'
						WHERE US.CurrentDate='{$date_current}' AND US.SymptomID=2;";
	
	$responseSymptom2Count = mysqli_query($connection,$sqlSymptom2Count);
	
	while($row = mysqli_fetch_array($responseSymptom2Count)){
		$symptom2 = $row["count"];
	}
	
	$sqlSymptom3Count = "SELECT COUNT(*) as count FROM usersymptoms US 
						INNER JOIN userinfo UI ON US.UserID=UI.UserID AND UI.Email='{$user_email}'
						WHERE US.CurrentDate='{$date_current}' AND US.SymptomID=3;";
	
	$responseSymptom3Count = mysqli_query($connection,$sqlSymptom3Count);
	
	while($row = mysqli_fetch_array($responseSymptom3Count)){
		$symptom3 = $row["count"];
	}
	
	$sqlSymptom4Count = "SELECT COUNT(*) as count FROM usersymptoms US 
						INNER JOIN userinfo UI ON US.UserID=UI.UserID AND UI.Email='{$user_email}'
						WHERE US.CurrentDate='{$date_current}' AND US.SymptomID=4;";
	
	$responseSymptom4Count = mysqli_query($connection,$sqlSymptom4Count);
	
	while($row = mysqli_fetch_array($responseSymptom4Count)){
		$symptom4 = $row["count"];
	}
	
	$sqlSymptom5Count = "SELECT COUNT(*) as count FROM usersymptoms US 
						INNER JOIN userinfo UI ON US.UserID=UI.UserID AND UI.Email='{$user_email}'
						WHERE US.CurrentDate='{$date_current}' AND US.SymptomID=5;";
	
	$responseSymptom5Count = mysqli_query($connection,$sqlSymptom5Count);
	
	while($row = mysqli_fetch_array($responseSymptom5Count)){
		$symptom5 = $row["count"];
	}
	
	array_push($overall_result,$symptom1); 
	array_push($overall_result,$symptom2); 
	array_push($overall_result,$symptom3); 
	array_push($overall_result,$symptom4); 
	array_push($overall_result,$symptom5); 
	
	
	
	echo json_encode($overall_result);
		
?>