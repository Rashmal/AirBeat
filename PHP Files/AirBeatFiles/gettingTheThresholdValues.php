<?php

	require_once 'db_connection.php';

	$user_email = $_POST['email'];
	
	$symptoms_id_list = array();
	
	$symptom_id1_value_list = array();
	$symptom_id2_value_list = array();
	$symptom_id3_value_list = array();
	$symptom_id4_value_list = array();
	$symptom_id5_value_list = array();
	$particle_value_list = array();
	
	
	//----------------- Gtting the symptom IDs based on the user -----------------
	
	$sql = "SELECT DISTINCT(US.SymptomID) AS 'SymptomsID' FROM usersymptoms US INNER JOIN userinfo UI ON UI.UserID=US.UserID AND UI.Email='{$user_email}';";
	
	$response = mysqli_query($connection,$sql);
	
	$overall_result = array();
	
	
	while($row = mysqli_fetch_array($response)){
		array_push($symptoms_id_list, $row["SymptomsID"]);  
	}
	
	//-----------------End of Gtting the symptom IDs based on the user -----------------
	
	//----------------- Getting the air reading values based on the symptom id ------------------
	
	

	foreach ($symptoms_id_list as $symptoms_id) {
		
		$sql_ar = "SELECT * FROM usersymptoms US 
					INNER JOIN airreading AR ON US.airreadingid = AR.ID
					INNER JOIN userinfo UI ON UI.UserID = US.UserID AND UI.Email='{$user_email}'
					WHERE US.SymptomID={$symptoms_id};";
		
		$response_ar = mysqli_query($connection,$sql_ar);
		
		while($row = mysqli_fetch_array($response_ar)){
			if($row["SymptomID"] == "1"){
				$result["Co"] = $row["Co"];
				$result["Particle"] = $row["Particle"];
				$result["Ozone"] = $row["Ozone"];
				$result["LPG"] = $row["LPG"];
				$result["Smoke"] = $row["Smoke"];
				array_push($symptom_id1_value_list, $result);  
			}
			if($row["SymptomID"] == "2"){
				$result["Co"] = $row["Co"];
				$result["Particle"] = $row["Particle"];
				$result["Ozone"] = $row["Ozone"];
				$result["LPG"] = $row["LPG"];
				$result["Smoke"] = $row["Smoke"];
				array_push($symptom_id2_value_list, $result);  
			}
			if($row["SymptomID"] == "3"){
				$result["Co"] = $row["Co"];
				$result["Particle"] = $row["Particle"];
				$result["Ozone"] = $row["Ozone"];
				$result["LPG"] = $row["LPG"];
				$result["Smoke"] = $row["Smoke"];
				array_push($symptom_id3_value_list, $result);  
			}
			if($row["SymptomID"] == "4"){
				$result["Co"] = $row["Co"];
				$result["Particle"] = $row["Particle"];
				$result["Ozone"] = $row["Ozone"];
				$result["LPG"] = $row["LPG"];
				$result["Smoke"] = $row["Smoke"];
				array_push($symptom_id4_value_list, $result);  
			}
			if($row["SymptomID"] == "5"){
				$result["Co"] = $row["Co"];
				$result["Particle"] = $row["Particle"];
				$result["Ozone"] = $row["Ozone"];
				$result["LPG"] = $row["LPG"];
				$result["Smoke"] = $row["Smoke"];
				array_push($symptom_id5_value_list, $result);  
			}
		}
	}	
	//----------------- End of Getting the air reading values based on the symptom id ------------------
	//$overall_result["Symptom1"]=array();
	//$overall_result["Symptom2"]=array();
	//$overall_result["Symptom3"]=array();
	//$overall_result["Symptom4"]=array();
	//$overall_result["Symptom5"]=array();
	array_push($overall_result,$symptom_id1_value_list); 
	array_push($overall_result,$symptom_id2_value_list); 
	array_push($overall_result,$symptom_id3_value_list); 
	array_push($overall_result,$symptom_id4_value_list); 
	array_push($overall_result,$symptom_id5_value_list); 
	
	echo json_encode($overall_result);
		
?>