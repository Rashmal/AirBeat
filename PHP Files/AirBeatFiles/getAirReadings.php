<?php

	require_once 'db_connection.php';
	
	$date_current = $_POST['date'];
	
	$sql = "SELECT 
			ROUND(IFNULL(AVG(airreading.Co),0),2) as 'CO_VLAUE',
			ROUND(IFNULL(AVG(airreading.Ozone),0),2) as 'OZONE_VLAUE',
			ROUND(IFNULL(AVG(airreading.LPG),0),2) as 'LPG_VLAUE',
			ROUND(IFNULL(AVG(airreading.Smoke),0),2) as 'SMOKE_VLAUE',
			ROUND(IFNULL(AVG(airreading.Particle),0),2) as 'PARTICLE_VLAUE'
			FROM airreading WHERE airreading.CurrentDate='{$date_current}';";
	
	$response = mysqli_query($connection,$sql);
	$res = mysqli_fetch_assoc($response);
	
	$result["CO_VLAUE"] = $res["CO_VLAUE"];
	$result["PARTICLE_VLAUE"] = $res["PARTICLE_VLAUE"];
	$result["LPG_VLAUE"] = $res["LPG_VLAUE"];
	$result["SMOKE_VLAUE"] = $res["SMOKE_VLAUE"];
	$result["OZONE_VLAUE"] = $res["OZONE_VLAUE"];
	

	
	
	echo json_encode($result);
		
?>