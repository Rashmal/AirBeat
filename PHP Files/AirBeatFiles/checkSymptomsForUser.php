<?php

	require_once 'db_connection.php';

	
	$airreadingID;
	$user_email = $_POST['email'];
	
	$symptom_ID_list = array();
	$overall_result = array();
	
	//Getting the last record id in air reading
	$sqlAirReadingID = "SELECT * FROM airreading ORDER BY airreading.ID DESC LIMIT 1;";
	
	$responseSymptom1Count = mysqli_query($connection,$sqlAirReadingID);
	
	while($row = mysqli_fetch_array($responseSymptom1Count)){
		$airreadingID = $row["ID"];
	}
	//end
	
	//----------------- Gtting the symptom IDs based on the user -----------------
	
	$sqlSymptomIDList = "SELECT DISTINCT(ST.SymptomID) AS 'SID' FROM symptomtrigger ST WHERE ST.UserEmail='{$user_email}';";
	
	$responseSymptomIDList = mysqli_query($connection,$sqlSymptomIDList);
	
	while($row = mysqli_fetch_array($responseSymptomIDList)){
		$symptom_ID = $row["SID"];
		array_push($symptom_ID_list,$symptom_ID); 
	}
	
	//------------------------------------------------------------
	
	foreach ($symptom_ID_list as $symptom_ID) {
		$sqlSymptomDetails = "SELECT * FROM symptomtrigger ST 
								INNER JOIN airreading AR ON AR.ID={$airreadingID}
								WHERE ST.SymptomID={$symptom_ID} AND ST.UserEmail='{$user_email}';";
		
		$responseSymptomDetails  = mysqli_query($connection,$sqlSymptomDetails);
		
		$factorDetails = array();
		array_push($factorDetails,$symptom_ID);
		
		while($row = mysqli_fetch_array($responseSymptomDetails)){
			$airreadingComponent = $row["airreadingComponent"];
			$thresholdValue =  $row["SymptomThreshold"];
			$upperThresholdValue = $thresholdValue +3;
			$lowerThresholdValue = $thresholdValue -3;
			
			$valueCo = $row["Co"];
			$valueParticle = $row["Particle"];
			$valueLpg = $row["LPG"];
			$valueSmoke = $row["Smoke"];
			$valueMethane = $row["Ozone"];
			
			if($airreadingComponent == "Co"){
				if($valueCo >= $lowerThresholdValue && $valueCo<= $upperThresholdValue){
					
				}else{
					if(copmponentsExists($symptom_ID,"Co",$overall_result)){
						
					}else{
						array_push($factorDetails,"Co");
					}
					
				}
			}
			
			if($airreadingComponent == "LPG"){
				if($valueParticle >= $lowerThresholdValue && $valueParticle<= $upperThresholdValue){
					
				}else{
					if(copmponentsExists($symptom_ID,"LPG",$overall_result)){
						
					}else{
						array_push($factorDetails,"LPG");
					}
					
				}
			}
			
			if($airreadingComponent == "Ozone"){
				if($valueMethane >= $lowerThresholdValue && $valueMethane<= $upperThresholdValue){
					
				}else{
					if(copmponentsExists($symptom_ID,"Ozone",$overall_result)){
						
					}else{
						array_push($factorDetails,"Ozone");
					}
					
				}
			}
			
			if($airreadingComponent == "Particle"){
				if($valueParticle >= $lowerThresholdValue && $valueParticle<= $upperThresholdValue){
					
				}else{
					if(copmponentsExists($symptom_ID,"Particle",$overall_result)){
						
					}else{
						array_push($factorDetails,"Particle");
					}
					
				}
			}
			
			if($airreadingComponent == "Smoke"){
				if($valueSmoke >= $lowerThresholdValue && $valueSmoke<= $upperThresholdValue){
					
				}else{
					if(copmponentsExists($symptom_ID,"Smoke",$overall_result)){
						
					}else{
						array_push($factorDetails,"Smoke");
					}
					
				}
			}
		}
		array_push($overall_result,$factorDetails);
	}
	
	
	function copmponentsExists($id,$component,$overall_result){
		$status = false;
		if(sizeof($overall_result)>0){
			foreach ($overall_result as $result){
				if($result[0] == $id){
					if($result[0][1]==$component){
						$status=true;
						break;
					}
				}
			}
		}
		return $status;
	}
	
	$final_result=array();
	
	foreach ($overall_result as $overall_result_indivitual) {
		$overall_result_size =sizeof($overall_result_indivitual);
		if($overall_result_size > 1){
			array_push($final_result,$overall_result_indivitual[0]);
			array_push($final_result,$overall_result_indivitual[1]);
		}
	}
	
	
	echo json_encode($final_result);
		
?>