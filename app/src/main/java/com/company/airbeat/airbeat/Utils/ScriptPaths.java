package com.company.airbeat.airbeat.Utils;

public class ScriptPaths {
    public static final String BASE_IP = "http://192.168.1.3/";
    public static final String REGISTER_URL = BASE_IP + "AirBeatFiles/registerUser.php";
    public static final String LOGIN_URL = BASE_IP + "AirBeatFiles/loginUser.php";
    public static final String EMAIL_EXISTS = BASE_IP + "AirBeatFiles/emailExists.php";
    public static final String GET_FULLNAME = BASE_IP + "AirBeatFiles/getFullName.php";
    public static final String INSERT_USER_SYMPTOMS = BASE_IP + "AirBeatFiles/insertUserSymptoms.php";
    public static final String GET_AIR_READINGS = BASE_IP + "AirBeatFiles/getAirReadings.php";
    public static final String GET_AIR_READINGS_FOR_DURATIONS = BASE_IP + "AirBeatFiles/getAirReadingsBasedOnDates.php";
    public static final String GET_BPM_READINGS_FOR_DURATIONS = BASE_IP + "AirBeatFiles/getBpmReadings.php";
    public static final String GET_THRESHOLLD_VALUE = BASE_IP + "AirBeatFiles/gettingTheThresholdValues.php";
    public static final String GET_SYMPTOMS_TABLE_VALUES = BASE_IP + "AirBeatFiles/getSymptomsTableValues.php";
    public static final String INSERT_SYMPTOMS_TRIGGER = BASE_IP + "AirBeatFiles/insertSymptomTrigger.php";
    public static final String CHECK_SYMPTOMS_OF_THE_USER = BASE_IP + "AirBeatFiles/checkSymptomsForUser.php";
    public static final String GET_SYMPTOM_COUNT = BASE_IP + "AirBeatFiles/getSymptomCount.php";
}
