package com.company.airbeat.airbeat.Controller;

public class SymptomsInfoController {
    //Attributes
    private int symptomID;
    private String name;
    private String symptomIdentifier;

    //Setter
    public void setSymptomID(int symptomID) {
        this.symptomID = symptomID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSymptomIdentifier(String symptomIdentifier) {
        this.symptomIdentifier = symptomIdentifier;
    }

    //Getters

    public int getSymptomID() {
        return symptomID;
    }

    public String getName() {
        return name;
    }

    public String getSymptomIdentifier() {
        return symptomIdentifier;
    }
}
