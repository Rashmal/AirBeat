package com.company.airbeat.airbeat;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.company.airbeat.airbeat.Controller.UserInfoController;
import com.company.airbeat.airbeat.Controller.UserSymptomController;

import java.util.ArrayList;
import java.util.Calendar;

public class SymptomsTable extends AppCompatActivity {

    public static final String MyPREFERENCES = "MyPrefs";
    private static final String TAG = "AirPollutionChart";
    Button refreshBtn, setDateBtn;
    TextView wwText, cText, saText, dmText, ctText, dateDisplay;
    static final int DIALOG_ID = 0;
    int year_x, month_x, day_x;

    Handler setStartDateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            showDialog(DIALOG_ID);
        }
    };

    Handler refreshDateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            refreshBtn();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptoms_table);

        wwText = (TextView) findViewById(R.id.wwText);
        cText = (TextView) findViewById(R.id.cText);
        saText = (TextView) findViewById(R.id.saText);
        dmText = (TextView) findViewById(R.id.dmText);
        ctText = (TextView) findViewById(R.id.ctText);
        dateDisplay = (TextView) findViewById(R.id.startDateText);

        final Calendar calendar = Calendar.getInstance();
        year_x = calendar.get(Calendar.YEAR);
        month_x = calendar.get(Calendar.MONTH);
        day_x = calendar.get(Calendar.DAY_OF_MONTH);


        refreshBtn = (Button) findViewById(R.id.refreshChartBtn);
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                refreshDateHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in setStartDateButton : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(runnable);
                thread.start();
            }
        });
        setDateBtn = (Button) findViewById(R.id.setStartDateBtn);
        setDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                setStartDateHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in setStartDateButton : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(runnable);
                thread.start();
            }
        });


    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID) {
            return new DatePickerDialog(this, dpickerListner, year_x, month_x, day_x);
        }

        return null;
    }

    private DatePickerDialog.OnDateSetListener dpickerListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            year_x = i;
            month_x = i1 + 1;
            day_x = i2;

            String monthStr = month_x + "";
            String dayStr = day_x + "";

            if (month_x < 10) {
                monthStr = "0" + month_x;
            }
            if (day_x < 10) {
                dayStr = "0" + day_x;
            }

            dateDisplay.setText(year_x + "-" + monthStr + "-" + dayStr);
        }
    };

    public void refreshBtn() {
        UserInfoController userInfoController = new UserInfoController();
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String email = "";
        email = sharedPreferences.getString("email", "");
        userInfoController.setEmail(email);

        ArrayList<String> symptomsList = new ArrayList<>();

        if (dateDisplay.getText().toString().equals("yyyy-mm-dd")) {
            Toast.makeText(this, "Error: Please select the dates", Toast.LENGTH_LONG).show();
        } else {
            UserSymptomController userSymptomController = new UserSymptomController(userInfoController);
            userSymptomController.setCurrentDate(dateDisplay.getText().toString());
            symptomsList = userSymptomController.getSymptomsTableValues();

            wwText.setText(symptomsList.get(0));
            cText.setText(symptomsList.get(1));
            saText.setText(symptomsList.get(2));
            dmText.setText(symptomsList.get(3));
            ctText.setText(symptomsList.get(4));
        }


    }
}
