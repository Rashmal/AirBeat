package com.company.airbeat.airbeat.Controller;

import com.company.airbeat.airbeat.Model.AirReadingModel;

import java.util.ArrayList;

public class AirReadingController {

    private int ID;
    private double particle;
    private double co;
    private double lpg;
    private double ozone;
    private double smoke;
    private String currentDate;
    private String currentTime;
    private String startDate;
    private String endDate;


    public AirReadingController() {
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public double getParticle() {
        return particle;
    }

    public void setParticle(double particle) {
        this.particle = particle;
    }

    public double getCo() {
        return co;
    }

    public void setCo(double co) {
        this.co = co;
    }

    public double getLpg() {
        return lpg;
    }

    public void setLpg(double lpg) {
        this.lpg = lpg;
    }

    public double getOzone() {
        return ozone;
    }

    public void setOzone(double ozone) {
        this.ozone = ozone;
    }

    public double getSmoke() {
        return smoke;
    }

    public void setSmoke(double smoke) {
        this.smoke = smoke;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }


    //Methods
    public AirReadingController getAirReadings(){
        AirReadingModel airReadingModel = new AirReadingModel();
        AirReadingController airReadingController = new AirReadingController();
        airReadingController=airReadingModel.getAirReading(this);
        return airReadingController;
    }

    public ArrayList<AirReadingController> getAirReadingsForDuration(){
        ArrayList<AirReadingController> airReadingControllerList = new ArrayList<>();
        AirReadingModel airReadingModel = new AirReadingModel();
        airReadingControllerList = airReadingModel.getAirReadingsForDuration(this);
        return airReadingControllerList;
    }
}
