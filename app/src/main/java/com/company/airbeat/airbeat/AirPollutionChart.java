package com.company.airbeat.airbeat;


import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


public class AirPollutionChart extends AppCompatActivity{

    Button co_chart_btn;
    Button particle_chart_btn;
    Button methane_chart_btn;
    Button lpg_chart_btn;
    Button smoke_chart_btn;

    Handler coBtnHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //Setting the intent to redirect the page
            Intent intent = new Intent(AirPollutionChart.this, AirPollutionCOValueChart.class);
            //Redirecting the page
            startActivity(intent);
        }
    };
    Handler particleBtnHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //Setting the intent to redirect the page
            Intent intent = new Intent(AirPollutionChart.this, AirPollutionParticleValueChart.class);
            //Redirecting the page
            startActivity(intent);
        }
    };
    Handler methaneBtnHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //Setting the intent to redirect the page
            Intent intent = new Intent(AirPollutionChart.this, AirPollutionMethaneValueChart.class);
            //Redirecting the page
            startActivity(intent);
        }
    };
    Handler lpgBtnHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //Setting the intent to redirect the page
            Intent intent = new Intent(AirPollutionChart.this, AirPollutionLpgValueChart.class);
            //Redirecting the page
            startActivity(intent);
        }
    };
    Handler smokeBtnHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //Setting the intent to redirect the page
            Intent intent = new Intent(AirPollutionChart.this, AirPollutionSmokeValueChart.class);
            //Redirecting the page
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_pollution_chart);

        co_chart_btn = (Button) findViewById(R.id.co_type_btn);
        co_chart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable threadRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                coBtnHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in trigger_chart_btn : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(threadRunnable);
                thread.start();
            }
        });
        particle_chart_btn = (Button) findViewById(R.id.particle_type_btn);
        particle_chart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable threadRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                particleBtnHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in trigger_chart_btn : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(threadRunnable);
                thread.start();
            }
        });
        methane_chart_btn = (Button) findViewById(R.id.methane_type_btn);
        methane_chart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable threadRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                methaneBtnHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in trigger_chart_btn : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(threadRunnable);
                thread.start();
            }
        });
        lpg_chart_btn = (Button) findViewById(R.id.lpg_type_btn);
        lpg_chart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable threadRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                lpgBtnHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in trigger_chart_btn : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(threadRunnable);
                thread.start();
            }
        });
        smoke_chart_btn = (Button) findViewById(R.id.smoke_type_btn);
        smoke_chart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable threadRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                smokeBtnHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in trigger_chart_btn : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(threadRunnable);
                thread.start();
            }
        });
    }

}
