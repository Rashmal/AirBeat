package com.company.airbeat.airbeat.Model;

import com.company.airbeat.airbeat.Controller.UserInfoController;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.CountDownLatch;

import static com.company.airbeat.airbeat.Utils.ScriptPaths.EMAIL_EXISTS;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_FULLNAME;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.LOGIN_URL;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.REGISTER_URL;


public class UserInfoModel {

    //Constructor
    public UserInfoModel() {
    }

    //--------------------- Methods ------------------------------
    public void registerUser(final UserInfoController userInfoController) {
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    URL url = new URL(REGISTER_URL);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getEmail(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("userpassword", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getUserPassword(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("firstName", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getFirstName(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("lastName", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getLastName(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                } catch (Exception ex) {
                    System.out.println("Error in registerUser in UserInfoModel Class : " + ex);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean emailExisits(final UserInfoController userInfoController) {
        final boolean[] emailExistsStr = {false};
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    URL url = new URL(EMAIL_EXISTS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getEmail(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    //return result;
                    if (result.equals("success")) {
                        emailExistsStr[0] = true;
                    }

                } catch (Exception e) {
                    System.out.println("Error in emailExisits in UserInfoModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return emailExistsStr[0];
    }

    public boolean loginUser(final UserInfoController userInfoController) {
        final boolean[] loginStatus = {false};
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    URL url = new URL(LOGIN_URL);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getEmail(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getUserPassword(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    //return result;
                    if (result.equals("success")) {
                        loginStatus[0] = true;
                    }

                } catch (Exception e) {
                    System.out.println("Error in loginUser in UserInfoModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return loginStatus[0];
    }

    public String getFullName(final UserInfoController userInfoController) {
        final String[] fullNameStr = new String[1];
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    URL url = new URL(GET_FULLNAME);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getEmail(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(userInfoController.getUserPassword(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    //return result;
                    fullNameStr[0] = result;


                } catch (Exception e) {
                    System.out.println("Error in loginUser in UserInfoModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return fullNameStr[0];
    }
}
