package com.company.airbeat.airbeat;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.icu.util.Calendar;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.company.airbeat.airbeat.Controller.LoginUser;
import com.company.airbeat.airbeat.Controller.UserInfoController;

import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private TextView app_name_textview;
    private Button register_btn;
    public static final String MyPREFERENCES = "MyPrefs";

    //Declaring and creating the handler method
    Handler loginHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            attemptLogin();
        }
    };

    Handler loginTitleTypeFaceHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Typeface app_title_typeface = Typeface.createFromAsset(getAssets(), "drako_heart.otf");
            app_name_textview.setTypeface(app_title_typeface);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        app_name_textview = (TextView) findViewById(R.id.app_name_text);
        register_btn = (Button) findViewById(R.id.register_button);

        if(checkLogin()){
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }

        loginTitleTypeFaceHandler.sendEmptyMessage(0);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                //Creating the runnable method
                Runnable loginRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                loginHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in login : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread loginThread = new Thread(loginRunnable);
                loginThread.start();
            }
        });

        register_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating the runnable method
                Runnable registerBtnRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                //Setting the intent to redirect the page
                                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                                //Redirecting the page
                                startActivity(intent);
                                finish();
                            } catch (Exception ex) {
                                System.out.println("Error in register button in login page : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread registerBtnThread = new Thread(registerBtnRunnable);
                registerBtnThread.start();

            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        UserInfoController userInfoController = new UserInfoController();
        userInfoController.setEmail(email);
        userInfoController.setUserPassword(password);

        boolean cancel = false;
        View focusView = null;

        // Check if the field is empty
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        } else if (!userInfoController.emailExists()) {
            mEmailView.setError(getString(R.string.error_email_does_not_exists));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            //The login function

            Boolean loginStatus = false;
            LoginUser loginUser = new LoginUser(userInfoController);
            try {
                loginStatus = loginUser.execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            if (loginStatus) {
                Toast.makeText(this, "Successfully Logged In", Toast.LENGTH_LONG).show();
                //Setting the intent to redirect the page

                //Creating the Shared Preference object
                SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                //Setting the values to shared preference
                editor.putString("email", email);
                editor.putString("full_name", userInfoController.getFullName());
                editor.commit();

                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                //Redirecting the page
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "Error: Login is unsuccessful", Toast.LENGTH_LONG).show();
                //Setting the intent to redirect the page
                Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                //Redirecting the page
                startActivity(intent);
                finish();
            }


        }
    }

    private boolean isEmailValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);

        return pat.matcher(email).matches();
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private boolean checkLogin(){
        boolean flag = false;
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String email="";
        if(sharedPreferences.contains("email")){
            flag = true;
        }
        return flag;
    }




}

