package com.company.airbeat.airbeat;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.company.airbeat.airbeat.Controller.UserInfoController;
import com.company.airbeat.airbeat.Controller.UserSymptomController;
import com.company.airbeat.airbeat.Utils.TriggerAlgorithm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TriggerChart extends AppCompatActivity {

    public static final String MyPREFERENCES = "MyPrefs";
    private static final String TAG = "AirPollutionChart";
    Button refreshBtn, setDateBtn;
    TextView dateDisplay, severityText;
    static final int DIALOG_ID = 0;
    int year_x, month_x, day_x;

    Handler setStartDateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            showDialog(DIALOG_ID);
        }
    };

    Handler refreshDateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            refreshBtn();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trigger_chart);

        dateDisplay = (TextView) findViewById(R.id.startDateText);
        severityText =  (TextView) findViewById(R.id.severityText);

        final Calendar calendar = Calendar.getInstance();
        year_x = calendar.get(Calendar.YEAR);
        month_x = calendar.get(Calendar.MONTH);
        day_x = calendar.get(Calendar.DAY_OF_MONTH);


        refreshBtn = (Button) findViewById(R.id.refreshChartBtn);
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                refreshDateHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in setStartDateButton : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(runnable);
                thread.start();
            }
        });
        setDateBtn = (Button) findViewById(R.id.setStartDateBtn);
        setDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                setStartDateHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in setStartDateButton : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(runnable);
                thread.start();
            }
        });
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID) {
            return new DatePickerDialog(this, dpickerListner, year_x, month_x, day_x);
        }

        return null;
    }

    private DatePickerDialog.OnDateSetListener dpickerListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            year_x = i;
            month_x = i1 + 1;
            day_x = i2;

            String monthStr = month_x + "";
            String dayStr = day_x + "";

            if (month_x < 10) {
                monthStr = "0" + month_x;
            }
            if (day_x < 10) {
                dayStr = "0" + day_x;
            }

            dateDisplay.setText(year_x + "-" + monthStr + "-" + dayStr);
        }
    };

    public void refreshBtn() {
        if (dateDisplay.getText().toString().equals("yyyy-mm-dd")) {
            Toast.makeText(this, "Error: Please select the dates", Toast.LENGTH_LONG).show();
        } else {


            String date = dateDisplay.getText().toString();
            Date date1 = null;
            try {
                date1 = new SimpleDateFormat("yyyy-mm-dd").parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 6);
            Date myDate = cal.getTime();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
            String endDate = dateFormat.format(myDate);

            SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            //Declaring variables
            String email="";
            email = sharedPreferences.getString("email","");

            TriggerAlgorithm triggerAlgorithm = new TriggerAlgorithm();
            String severity = triggerAlgorithm.getSeverityBasedOnDuration(email,endDate,date);

            //Setting the severity
            severityText.setText(severity);

            //----------------------Adding rows to the table dynamically ----------------------

            /*
            cleanTable(triggerTableID);

            TableRow tr_head = new TableRow(this);
            tr_head.setBackgroundColor(Color.GRAY);
            tr_head.setLayoutParams(new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.FILL_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));

            TextView label_date = new TextView(this);
            label_date.setText("DATE");
            label_date.setGravity(Gravity.CENTER | Gravity.BOTTOM);
            ;
            label_date.setTextColor(Color.WHITE);
            label_date.setPadding(5, 5, 5, 5);
            tr_head.addView(label_date);// add the column to the table row here

            TextView label_weight_kg = new TextView(this);
            label_weight_kg.setText("Wt(Kg.)"); // set the text for the header
            label_weight_kg.setGravity(Gravity.CENTER | Gravity.BOTTOM);
            ;
            label_weight_kg.setTextColor(Color.WHITE); // set the color
            label_weight_kg.setPadding(5, 5, 5, 5); // set the padding (if required)
            tr_head.addView(label_weight_kg); // add the column to the table row here

            triggerTableID.addView(tr_head, new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.FILL_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            //----------------------End of Adding rows to the table dynamically ----------------------
            */
        }
    }

    private void cleanTable(TableLayout table) {

        int childCount = table.getChildCount();

        // Remove all rows except the first one
        if (childCount > 1) {
            table.removeViews(1, childCount - 1);
        }
    }

}
