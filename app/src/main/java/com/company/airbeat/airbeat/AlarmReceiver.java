package com.company.airbeat.airbeat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Uri sound =  RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager mNM = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);
        Intent intentI = new Intent(context, LoginActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,100,intentI,PendingIntent.FLAG_UPDATE_CURRENT);

        String CHANNEL_ID = "my_channel_01";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            mNM.createNotificationChannel(mChannel);
        }


        Notification notification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationCompat.Builder notificationBuilder  = new NotificationCompat.Builder(context,CHANNEL_ID)
                    .setContentTitle("Dont forget to update your health details")
                    .setContentText("Click here to open AirBeat").setSmallIcon(android.support.v4.R.drawable.notify_panel_notification_icon_bg)
                    .setSound(sound)
                    .setContentIntent(pendingIntent)
                    .addAction(0,"Load APP",pendingIntent);
            mNM.notify(100,notificationBuilder.build());
        }

    }
}
