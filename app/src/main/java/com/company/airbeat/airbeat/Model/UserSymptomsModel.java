package com.company.airbeat.airbeat.Model;

import com.company.airbeat.airbeat.Controller.AirReadingController;
import com.company.airbeat.airbeat.Controller.BpmController;
import com.company.airbeat.airbeat.Controller.SymptomController;
import com.company.airbeat.airbeat.Controller.SymptomsInfoController;
import com.company.airbeat.airbeat.Controller.UserSymptomController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_SYMPTOMS_TABLE_VALUES;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_SYMPTOM_COUNT;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_THRESHOLLD_VALUE;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.INSERT_USER_SYMPTOMS;

public class UserSymptomsModel {
    //Constructor

    public UserSymptomsModel() {
    }

    //Methods
    public void insertSelectedSymptoms(final UserSymptomController userSymptomController){
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    JSONArray jsArray = new JSONArray(userSymptomController.getSymptomIdentifiers());
                    String symptomsList = jsArray.toString();


                    URL url = new URL(INSERT_USER_SYMPTOMS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getUserInfoController().getEmail(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("symptoms", "UTF-8") + "=" + URLEncoder.encode(symptomsList, "UTF-8")
                            + "&" +
                            URLEncoder.encode("currentDate", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getCurrentDate(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("currentTime", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getCurrentTime(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));

                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                } catch (Exception ex) {
                    System.out.println("Error in insertSelectedSymptoms in UserSymptomsModel Class : " + ex);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<SymptomController> getThresholdValue(final UserSymptomController userSymptomController){
        final String[] resultString = new String[1];
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    //JSONArray jsArray = new JSONArray(userSymptomController.getSymptomIdentifiers());
                    //String symptomsList = jsArray.toString();


                    URL url = new URL(GET_THRESHOLLD_VALUE);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getUserInfoController().getEmail(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));

                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    //return result;
                    resultString[0] = result;

                } catch (Exception ex) {
                    System.out.println("Error in insertSelectedSymptoms in UserSymptomsModel Class : " + ex);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        JSONArray obj = null;
        try {
            obj = new JSONArray(resultString[0]);


        } catch (Exception e) {
            e.printStackTrace();
        }


        ArrayList<SymptomController> symptomControllerList = new ArrayList<>();

        //Iterating the jason array
        int jsonArraySize = obj.length();
        String kk="";

        for(int i=0;i<jsonArraySize;i++){
            SymptomController symptomController  = new SymptomController();

            symptomController.setSymptomID(i+1);

            int symptomsArraySize = 0;

            try {
                symptomsArraySize = obj.getJSONArray(i).length();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for(int k=0;k<symptomsArraySize;k++){
                AirReadingController airReadingController = new AirReadingController();

                JSONObject jsonObject=null;

                try {
                    jsonObject = new JSONObject(obj.getJSONArray(i).get(k).toString());
                    airReadingController.setCo(jsonObject.getDouble("Co"));
                    airReadingController.setLpg(jsonObject.getDouble("LPG"));
                    airReadingController.setParticle(jsonObject.getDouble("Particle"));
                    airReadingController.setOzone(jsonObject.getDouble("Ozone"));
                    airReadingController.setSmoke(jsonObject.getDouble("Smoke"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                symptomController.addAirReadingObj(airReadingController);

            }
            symptomControllerList.add(symptomController);
        }
        return symptomControllerList;
    }


    public ArrayList<String> getSymptomsTableValues(final UserSymptomController userSymptomController){
        final String[] resultString = new String[1];
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    //JSONArray jsArray = new JSONArray(userSymptomController.getSymptomIdentifiers());
                    //String symptomsList = jsArray.toString();


                    URL url = new URL(GET_SYMPTOMS_TABLE_VALUES);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getCurrentDate(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getUserInfoController().getEmail(), "UTF-8") ;
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));

                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    //return result;
                    resultString[0] = result;

                } catch (Exception ex) {
                    System.out.println("Error in insertSelectedSymptoms in UserSymptomsModel Class : " + ex);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        JSONArray obj = null;
        try {
            obj = new JSONArray(resultString[0]);


        } catch (Exception e) {
            e.printStackTrace();
        }


        ArrayList<String> symptomControllerList = new ArrayList<>();

        //Iterating the jason array
        int jsonArraySize = obj.length();

        for(int i=0;i<jsonArraySize;i++){

            try {
                symptomControllerList.add(obj.get(i).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return symptomControllerList;
    }

    public int getCount(final UserSymptomController userSymptomController){
        final int[] countint = new int[1];
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(GET_SYMPTOM_COUNT);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getEmail(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("currentdate", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getCurrentDate(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("previousdate", "UTF-8") + "=" + URLEncoder.encode(userSymptomController.getPreviousDate(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    //return result;
                    countint[0] = Integer.parseInt(result);


                } catch (Exception e) {
                    System.out.println("Error in loginUser in UserInfoModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return countint[0];
    }

}
