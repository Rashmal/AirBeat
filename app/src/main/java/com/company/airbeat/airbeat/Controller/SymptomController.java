package com.company.airbeat.airbeat.Controller;

import com.company.airbeat.airbeat.Model.AirReadingModel;
import com.company.airbeat.airbeat.Model.UserSymptomsModel;
import com.company.airbeat.airbeat.Utils.ThresholdAlgo;

import java.util.ArrayList;
import java.util.HashMap;

public class SymptomController {
    private int symptomID;
    ArrayList<AirReadingController> airReadingControllersList = new ArrayList<>();
    ArrayList<SymptomController> symptomControllersList = new ArrayList<>();



    public int getSymptomID() {
        return symptomID;
    }

    public void setSymptomID(int symptomID) {
        this.symptomID = symptomID;
    }

    public ArrayList<AirReadingController> getAirReadingControllersList() {
        return airReadingControllersList;
    }

    public void addAirReadingObj(AirReadingController airReadingController){
        this.airReadingControllersList.add(airReadingController);
    }

    //Methods
    public ArrayList<HashMap<String, Double>> getThresholdValue(String email){
        UserInfoController userInfoController = new UserInfoController();
        userInfoController.setEmail(email);

        UserSymptomController userSymptomController = new UserSymptomController(userInfoController);
        UserSymptomsModel userSymptomsModel = new UserSymptomsModel();
        symptomControllersList = userSymptomsModel.getThresholdValue(userSymptomController);
        ThresholdAlgo thresholdAlgo = new ThresholdAlgo();
        thresholdAlgo.setSymptomControllersList(symptomControllersList);
        return thresholdAlgo.performAlgo();

    }


}
