package com.company.airbeat.airbeat.Model;

import com.company.airbeat.airbeat.Controller.AirReadingController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_AIR_READINGS;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_AIR_READINGS_FOR_DURATIONS;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_FULLNAME;

public class AirReadingModel {

    public AirReadingController getAirReading(final AirReadingController airReadingController) {
        AirReadingController resultAirReadingController1 = new AirReadingController();

        final String[] resultString = new String[1];
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    URL url = new URL(GET_AIR_READINGS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(airReadingController.getCurrentDate(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    //return result;
                    resultString[0] = result;


                } catch (Exception e) {
                    System.out.println("Error in getAirReading in AirReadingModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        JSONObject obj = null;
        try {
            obj = new JSONObject(resultString[0]);
            resultAirReadingController1.setCo(obj.getDouble("CO_VLAUE"));
            resultAirReadingController1.setOzone(obj.getDouble("OZONE_VLAUE"));
            resultAirReadingController1.setParticle(obj.getDouble("PARTICLE_VLAUE"));
            resultAirReadingController1.setLpg(obj.getDouble("LPG_VLAUE"));
            resultAirReadingController1.setSmoke(obj.getDouble("SMOKE_VLAUE"));
        } catch (Exception e) {
            e.printStackTrace();
        }



        return resultAirReadingController1;
    }

    public ArrayList<AirReadingController> getAirReadingsForDuration(final AirReadingController airReadingController){
        final String[] resultString = new String[1];
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    URL url = new URL(GET_AIR_READINGS_FOR_DURATIONS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(airReadingController.getStartDate(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    //return result;
                    resultString[0] = result;


                } catch (Exception e) {
                    System.out.println("Error in getAirReadingsForDuration in AirReadingModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        JSONArray obj = null;
        try {
            obj = new JSONArray(resultString[0]);


        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<AirReadingController> airReadingControllerList = new ArrayList<>();

        //Iterating the jason array
        int jsonArraySize = obj.length();
        String kk="";

        for(int i=0;i<jsonArraySize;i++){
            AirReadingController airReadingController1 = new AirReadingController();
            JSONObject jsonObject=null;
            try {
                jsonObject = new JSONObject(obj.get(i).toString());
                airReadingController1.setCo(jsonObject.getDouble("CO_VLAUE"));
                airReadingController1.setOzone(jsonObject.getDouble("OZONE_VLAUE"));
                airReadingController1.setParticle(jsonObject.getDouble("PARTICLE_VLAUE"));
                airReadingController1.setLpg(jsonObject.getDouble("LPG_VLAUE"));
                airReadingController1.setSmoke(jsonObject.getDouble("SMOKE_VLAUE"));
                airReadingController1.setCurrentTime(jsonObject.getString("CurrentTime")+"");

                airReadingControllerList.add(airReadingController1);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return airReadingControllerList;
    }

}
