package com.company.airbeat.airbeat.Controller;

import com.company.airbeat.airbeat.Model.TriggerSymptomsModel;

import java.util.ArrayList;

public class TriggerSymptomsController {

    private int triggerID;
    private int symptomID;
    private double symptomThreshold;
    private String airreadingComponent;
    private String userEmail;
    private TriggerSymptomsModel triggerSymptomsModel;


    public String getAirreadingComponent() {
        return airreadingComponent;
    }

    public void setAirreadingComponent(String airreadingComponent) {
        this.airreadingComponent = airreadingComponent;
    }

    public int getTriggerID() {
        return triggerID;
    }

    public void setTriggerID(int triggerID) {
        this.triggerID = triggerID;
    }

    public int getSymptomID() {
        return symptomID;
    }

    public void setSymptomID(int symptomID) {
        this.symptomID = symptomID;
    }

    public double getSymptomThreshold() {
        return symptomThreshold;
    }

    public void setSymptomThreshold(double symptomThreshold) {
        this.symptomThreshold = symptomThreshold;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void insertRecord(){
        triggerSymptomsModel = new TriggerSymptomsModel();
        this.triggerSymptomsModel.insertRecord(this);
    }

    public ArrayList<String> checkTheSmptomsOfTheUser(){
        TriggerSymptomsModel triggerSymptomsModel = new TriggerSymptomsModel();
        return triggerSymptomsModel.checkTheSmptomsOfTheUser(this);
    }

}
