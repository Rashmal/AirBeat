package com.company.airbeat.airbeat;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Set;


public class SetAlarmActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //----------------------------Navigation Drawer items--------------------------------
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView userNameView;
    private TextView userEmailView;
    private MenuItem nav_logout_menu;
    private MenuItem nav_basic_info_menu;
    private MenuItem nav_bpm_menu;
    private MenuItem nav_symptoms_menu;
    private MenuItem nav_charts_menu;
    private MenuItem nav_home_menu;
    private MenuItem nav_trigger_menu;
    private MenuItem nav_set_alarm_menu;
    public static final String MyPREFERENCES = "MyPrefs";


    //---------------------------- Handlers ------------------------------------------
    Handler setNavigationHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setNavigationViewListner();
        }
    };
    Handler setSharedPreferenceValuesHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setSharedPreferenceValues();
        }
    };
    //----------------------------End of Handlers ------------------------------------
    //----------------------------End of navigation items----------------------------

    private AlarmManager alarmManager;
    private TimePicker alarm_timePicker;
    private TextView update_alarm_textView;
    private Button alarm_on;
    private Button alarm_off;
    private Context context;
    private Calendar calendar;
    private PendingIntent pendingIntent;

    //Declaring and creating the handler method
    Handler alarmOnHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            alarmOnFunction();
        }
    };
    Handler alarmOffHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            alarmOffFunction();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);
        this.context = this;

        //----------------------------Navigation components----------------------------
        setNavigationHandler.sendEmptyMessage(0);

        drawerLayout = (DrawerLayout) findViewById(R.id.set_alarm_page);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav = navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_basic_info_menu = menuNav.findItem(R.id.basic_info_menu);
        nav_bpm_menu = menuNav.findItem(R.id.bpm_menu);
        nav_symptoms_menu = menuNav.findItem(R.id.symptoms_menu);
        nav_charts_menu = menuNav.findItem(R.id.charts_menu);
        nav_trigger_menu = menuNav.findItem(R.id.trigger_menu);
        nav_home_menu = menuNav.findItem(R.id.home_menu);
        nav_set_alarm_menu = menuNav.findItem(R.id.set_alarm_menu);


        setSharedPreferenceValuesHandler.sendEmptyMessage(0);

        //----------------------------End of navigation components----------------------------

        //Initialize the alarm manager
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        //Initialize time picker
        alarm_timePicker = (TimePicker) findViewById(R.id.time_picker_id);
        //Initialize the text view
        update_alarm_textView = (TextView) findViewById(R.id.update_alarm_text);
        //Initialize an instance of the calender
        calendar = Calendar.getInstance();
        //Initialize the buttons
        alarm_on = (Button) findViewById(R.id.alarm_on_btn);
        alarm_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating the runnable method
                Runnable alarmOnRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                alarmOnHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in alarm on : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread alarmOnThread = new Thread(alarmOnRunnable);
                alarmOnThread.start();
            }
        });
        alarm_off = (Button) findViewById(R.id.alarm_off_btn);
        alarm_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating the runnable method
                Runnable alarmOffRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                alarmOffHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in alarm off : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread alarmOffThread = new Thread(alarmOffRunnable);
                alarmOffThread.start();
            }
        });


    }
    //----------------------------Navigation methods----------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues() {
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname = "";
        String email = "";

        email = sharedPreferences.getString("email", "");
        fullname = sharedPreferences.getString("full_name", "");


        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                homeFunction();
                break;
            }

            case R.id.basic_info_menu: {
                //Calling the basic info function
                basicInfoFunction();
                break;
            }

            case R.id.bpm_menu: {
                //Calling the bpm function
                bpmFunction();
                break;
            }

            case R.id.symptoms_menu: {
                //Calling the symptoms function
                symptomsFunction();
                break;
            }

            case R.id.charts_menu: {
                //Calling the charts function
                chartsFunction();
                break;
            }

            case R.id.set_alarm_menu: {
                //Calling the charts function
                setAlarmFunction();
                break;
            }

            case R.id.trigger_menu: {
                //Calling the my trigger function
                triggersFunction();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAlarmFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SetAlarmActivity.this, SetAlarmActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the home function for the navigation
    public void homeFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SetAlarmActivity.this, HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bsic info function for the navigation
    public void basicInfoFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SetAlarmActivity.this, BasicInfoActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bpm function for the navigation
    public void bpmFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SetAlarmActivity.this, BpmActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the symptoms function for the navigation
    public void symptomsFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SetAlarmActivity.this, SymptomsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the charts function for the navigation
    public void chartsFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SetAlarmActivity.this, ChartsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the triggers function for the navigation
    public void triggersFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SetAlarmActivity.this, TriggerActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }


    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void logout_function() {
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(SetAlarmActivity.this, LoginActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //----------------------------End of navigation methods----------------------------

    //Set text function
    public void set_alarm_text(String output) {
        update_alarm_textView.setText(output);
    }


    //Button onclick functions
    public void alarmOnFunction() {

        //getting the selected time
        calendar.set(Calendar.HOUR_OF_DAY, alarm_timePicker.getHour());
        calendar.set(Calendar.MINUTE, alarm_timePicker.getMinute());

        int hour = alarm_timePicker.getHour();
        int minute = alarm_timePicker.getMinute();

        String hour_string = String.valueOf(hour);
        String minute_string = String.valueOf(minute);

        if (hour > 12) {
            hour_string = String.valueOf(hour - 12);
        }

        if (hour < 12) {
            hour_string = "0" + String.valueOf(hour);
        }

        if (minute < 10) {
            minute_string = "0" + String.valueOf(minute);
        }

        Intent intent = new Intent(this.context, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(SetAlarmActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

        this.set_alarm_text("Alarm set to: " + hour_string + ":" + minute_string);
    }

    public void alarmOffFunction() {
        this.set_alarm_text("Alarm off!!");
        alarmManager.cancel(pendingIntent);
    }

}
