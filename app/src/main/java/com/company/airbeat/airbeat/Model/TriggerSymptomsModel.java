package com.company.airbeat.airbeat.Model;

import com.company.airbeat.airbeat.Controller.TriggerSymptomsController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import static com.company.airbeat.airbeat.Utils.ScriptPaths.CHECK_SYMPTOMS_OF_THE_USER;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_SYMPTOMS_TABLE_VALUES;
import static com.company.airbeat.airbeat.Utils.ScriptPaths.INSERT_SYMPTOMS_TRIGGER;

public class TriggerSymptomsModel {

    public TriggerSymptomsModel() {
    }

    public void insertRecord(final TriggerSymptomsController triggerSymptomsController) {
        final boolean[] emailExistsStr = {false};
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL url = new URL(INSERT_SYMPTOMS_TRIGGER);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("userEmail", "UTF-8") + "=" + URLEncoder.encode(triggerSymptomsController.getUserEmail(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("symptomID", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(triggerSymptomsController.getSymptomID()), "UTF-8")
                            + "&" +
                            URLEncoder.encode("threshold", "UTF-8") + "=" + URLEncoder.encode(Double.toString(triggerSymptomsController.getSymptomThreshold()), "UTF-8")
                            + "&" +
                            URLEncoder.encode("airreadingcomponent", "UTF-8") + "=" + URLEncoder.encode(triggerSymptomsController.getAirreadingComponent(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                } catch (Exception e) {
                    System.out.println("Error in insertRecord in TriggerSymptomsModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> checkTheSmptomsOfTheUser(final TriggerSymptomsController triggerSymptomsController) {
        final String[] resultString = new String[1];
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL url = new URL(CHECK_SYMPTOMS_OF_THE_USER);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(triggerSymptomsController.getUserEmail(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));

                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    //return result;
                    resultString[0] = result;

                } catch (Exception e) {
                    System.out.println("Error in insertRecord in TriggerSymptomsModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        JSONArray obj = null;
        try {
            obj = new JSONArray(resultString[0]);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //Iterating the jason array
        int jsonArraySize = obj.length();
        ArrayList<String> symptomsList = new ArrayList<>();

        for(int i=0;i<jsonArraySize;i++){
            try {
                symptomsList.add(obj.getString(i).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        String str="";
        return symptomsList;
    }
}
