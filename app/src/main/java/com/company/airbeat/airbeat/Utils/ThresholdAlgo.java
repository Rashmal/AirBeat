package com.company.airbeat.airbeat.Utils;

import com.company.airbeat.airbeat.Controller.AirReadingController;
import com.company.airbeat.airbeat.Controller.SymptomController;

import java.util.ArrayList;
import java.util.HashMap;

public class ThresholdAlgo {
    ArrayList<SymptomController> symptomControllersList = new ArrayList<>();
    ArrayList<SymptomController> symptomDifferencesList = new ArrayList<>();

    ArrayList<HashMap<String, Double>> hashmapMeansArrayList = new ArrayList<HashMap<String, Double>>();
    ArrayList<HashMap<String, Double>> totalVarianceList = new ArrayList<HashMap<String, Double>>();
    ArrayList<HashMap<String, Double>> standardDeviationList = new ArrayList<HashMap<String, Double>>();
    ArrayList<HashMap<String, Double>> thresholdValueList = new ArrayList<HashMap<String, Double>>();

    HashMap<String, Double> symptomID01 = new HashMap<>();
    HashMap<String, Double> symptomID02 = new HashMap<>();
    HashMap<String, Double> symptomID03 = new HashMap<>();
    HashMap<String, Double> symptomID04 = new HashMap<>();
    HashMap<String, Double> symptomID05 = new HashMap<>();


    public ArrayList<SymptomController> getSymptomControllersList() {
        return symptomControllersList;
    }

    public void setSymptomControllersList(ArrayList<SymptomController> symptomControllersList) {
        this.symptomControllersList = symptomControllersList;
    }

    public ArrayList<HashMap<String, Double>> performAlgo() {
        addHashMapsToArrayList();
        this.hashmapMeansArrayList = calcMeanForAll();
        this.symptomDifferencesList = calcTheDifferences();
        makeDifferencesIntoThePowerOfTwo();
        this.totalVarianceList = calculateTotalVariance();
        this.standardDeviationList = calcStandardDeviation();
        return this.thresholdValueList = calcThresholdValues();
    }

    //-------------------Add Hashmaps to an arraylist-------------------
    public void addHashMapsToArrayList() {
        hashmapMeansArrayList.add(symptomID01);
        hashmapMeansArrayList.add(symptomID02);
        hashmapMeansArrayList.add(symptomID03);
        hashmapMeansArrayList.add(symptomID04);
        hashmapMeansArrayList.add(symptomID05);
    }

    //-------------------End of Add Hashmaps to an arraylist-------------------

    //---------------------Calculating the mean of air reading components---------------------
    public ArrayList<HashMap<String, Double>> calcMeanForAll() {
        ArrayList<HashMap<String, Double>> hashmapArrayListResult = new ArrayList<>();
        for (int i = 0; i < symptomControllersList.size(); i++) {
            SymptomController symptomController = new SymptomController();
            symptomController = symptomControllersList.get(i);

            double coMean = 0;
            double particlemean = 0;
            double lpgMean = 0;
            double smokeMean = 0;
            double ozoneMean = 0;
            double totalSumCo = 0;
            double totalSumParticle = 0;
            double totalSumLpg = 0;
            double totalSumSmoke = 0;
            double totalSumOzone = 0;

            for (int k = 0; k < symptomController.getAirReadingControllersList().size(); k++) {
                totalSumCo += symptomController.getAirReadingControllersList().get(k).getCo();
                totalSumParticle += symptomController.getAirReadingControllersList().get(k).getParticle();
                totalSumLpg += symptomController.getAirReadingControllersList().get(k).getLpg();
                totalSumSmoke += symptomController.getAirReadingControllersList().get(k).getSmoke();
                totalSumOzone += symptomController.getAirReadingControllersList().get(k).getOzone();
            }
            //Getting the mean values
            coMean = totalSumCo / symptomController.getAirReadingControllersList().size();
            particlemean = totalSumParticle / symptomController.getAirReadingControllersList().size();
            lpgMean = totalSumLpg / symptomController.getAirReadingControllersList().size();
            smokeMean = totalSumSmoke / symptomController.getAirReadingControllersList().size();
            ozoneMean = totalSumOzone / symptomController.getAirReadingControllersList().size();

            HashMap<String, Double> symptomHashMap = new HashMap<>();
            symptomHashMap = hashmapMeansArrayList.get(i);
            symptomHashMap.put("coMean", coMean);
            symptomHashMap.put("particleMean", particlemean);
            symptomHashMap.put("lpgMean", lpgMean);
            symptomHashMap.put("smokeMean", smokeMean);
            symptomHashMap.put("ozoneMean", ozoneMean);

            hashmapArrayListResult.add(symptomHashMap);
        }
        return hashmapArrayListResult;
    }
    //---------------------End of Calculating the mean of air reading components---------------------

    //--------------------- Get the differences with compared to mean ---------------------

    public ArrayList<SymptomController> calcTheDifferences() {
        ArrayList<SymptomController> symptomDifferencesList = new ArrayList<>();

        for (int i = 0; i < symptomControllersList.size(); i++) {
            SymptomController symptomController = new SymptomController();
            symptomController = symptomControllersList.get(i);
            SymptomController symptomDifferenceControllerResult = new SymptomController();

            for (int k = 0; k < symptomController.getAirReadingControllersList().size(); k++) {

                AirReadingController airReadingMeanController = new AirReadingController();

                double differenceValueCo = this.hashmapMeansArrayList.get(i).get("coMean") - symptomController.getAirReadingControllersList().get(k).getCo();
                airReadingMeanController.setCo(differenceValueCo);
                double differenceValueParticle = this.hashmapMeansArrayList.get(i).get("particleMean") - symptomController.getAirReadingControllersList().get(k).getParticle();
                airReadingMeanController.setParticle(differenceValueParticle);
                double differenceValueLpg = this.hashmapMeansArrayList.get(i).get("lpgMean") - symptomController.getAirReadingControllersList().get(k).getLpg();
                airReadingMeanController.setLpg(differenceValueLpg);
                double differenceValueSmoke = this.hashmapMeansArrayList.get(i).get("smokeMean") - symptomController.getAirReadingControllersList().get(k).getSmoke();
                airReadingMeanController.setSmoke(differenceValueSmoke);
                double differenceValueOzone = this.hashmapMeansArrayList.get(i).get("ozoneMean") - symptomController.getAirReadingControllersList().get(k).getOzone();
                airReadingMeanController.setOzone(differenceValueOzone);

                symptomDifferenceControllerResult.addAirReadingObj(airReadingMeanController);
            }
            symptomDifferencesList.add(symptomDifferenceControllerResult);
        }
        return symptomDifferencesList;
    }
    //--------------------- End of Get the differences with compared to mean ---------------------

    // ----------------------  Make the differences * differences --------------------------------

    public void makeDifferencesIntoThePowerOfTwo() {
        ArrayList<SymptomController> symptomDifferencesListNew = new ArrayList<>();
        for (int i = 0; i < symptomDifferencesList.size(); i++) {
            SymptomController symptomController = new SymptomController();
            symptomController = symptomDifferencesList.get(i);

            SymptomController symptomDifferenceControllerResult = new SymptomController();

            for (int k = 0; k < symptomController.getAirReadingControllersList().size(); k++) {
                AirReadingController airReadingMeanController = new AirReadingController();

                double currentCovalue = symptomController.getAirReadingControllersList().get(k).getCo();
                double newCoValue = currentCovalue * currentCovalue;
                airReadingMeanController.setCo(newCoValue);

                double currentParticlevalue = symptomController.getAirReadingControllersList().get(k).getParticle();
                double newParticleValue = currentParticlevalue * currentParticlevalue;
                airReadingMeanController.setParticle(newParticleValue);

                double currentLpgvalue = symptomController.getAirReadingControllersList().get(k).getLpg();
                double newLpgValue = currentLpgvalue * currentLpgvalue;
                airReadingMeanController.setLpg(newLpgValue);

                double currentSmokevalue = symptomController.getAirReadingControllersList().get(k).getSmoke();
                double newSmokeValue = currentSmokevalue * currentSmokevalue;
                airReadingMeanController.setSmoke(newSmokeValue);

                double currentOzonevalue = symptomController.getAirReadingControllersList().get(k).getOzone();
                double newOzoneValue = currentOzonevalue * currentOzonevalue;
                airReadingMeanController.setOzone(newOzoneValue);

                symptomDifferenceControllerResult.addAirReadingObj(airReadingMeanController);
            }
            symptomDifferencesListNew.add(symptomDifferenceControllerResult);
        }

        this.symptomDifferencesList = symptomDifferencesListNew;
    }

    // ---------------------- End of Make the differences * differences  --------------------------------

    // ----------------------  Calculate total variance --------------------------------

    public ArrayList<HashMap<String, Double>> calculateTotalVariance() {
        ArrayList<HashMap<String, Double>> totalVarianceList = new ArrayList<HashMap<String, Double>>();
        for (int i = 0; i < symptomDifferencesList.size(); i++) {
            SymptomController symptomController = new SymptomController();
            symptomController = symptomDifferencesList.get(i);

            double totalVarianceCo = 0;
            double totalVarianceParticle = 0;
            double totalVarianceSmoke = 0;
            double totalVarianceLpg = 0;
            double totalVarianceOzone = 0;

            for (int k = 0; k < symptomController.getAirReadingControllersList().size(); k++) {
                totalVarianceCo += symptomController.getAirReadingControllersList().get(k).getCo();
                totalVarianceParticle += symptomController.getAirReadingControllersList().get(k).getParticle();
                totalVarianceSmoke += symptomController.getAirReadingControllersList().get(k).getSmoke();
                totalVarianceLpg += symptomController.getAirReadingControllersList().get(k).getLpg();
                totalVarianceOzone += symptomController.getAirReadingControllersList().get(k).getOzone();
            }
            totalVarianceCo = totalVarianceCo / symptomController.getAirReadingControllersList().size();
            totalVarianceParticle = totalVarianceParticle / symptomController.getAirReadingControllersList().size();
            totalVarianceSmoke = totalVarianceSmoke / symptomController.getAirReadingControllersList().size();
            totalVarianceLpg = totalVarianceLpg / symptomController.getAirReadingControllersList().size();
            totalVarianceOzone = totalVarianceOzone / symptomController.getAirReadingControllersList().size();

            HashMap<String, Double> symptomVarianceHashMap = new HashMap<>();
            symptomVarianceHashMap.put("totalVarianceCo", totalVarianceCo);
            symptomVarianceHashMap.put("totalVarianceSmoke", totalVarianceSmoke);
            symptomVarianceHashMap.put("totalVarianceLpg", totalVarianceLpg);
            symptomVarianceHashMap.put("totalVarianceParticle", totalVarianceParticle);
            symptomVarianceHashMap.put("totalVarianceOzone", totalVarianceOzone);

            totalVarianceList.add(symptomVarianceHashMap);

        }

        return totalVarianceList;
    }

    // ---------------------- End of Calculate total variance --------------------------------

    // ----------------------  Calculate Standard Deviation --------------------------------

    public ArrayList<HashMap<String, Double>> calcStandardDeviation() {
        ArrayList<HashMap<String, Double>> standardDeviationList = new ArrayList<HashMap<String, Double>>();
        for (int i = 0; i < totalVarianceList.size(); i++) {
            HashMap<String, Double> varianceHashmap = new HashMap<String, Double>();
            varianceHashmap = totalVarianceList.get(i);
            HashMap<String, Double> standardDeviationHashmap = new HashMap<String, Double>();

            double standardDeviationCo = Math.sqrt(varianceHashmap.get("totalVarianceCo"));
            double standardDeviationParticle = Math.sqrt(varianceHashmap.get("totalVarianceParticle"));
            double standardDeviationSmoke = Math.sqrt(varianceHashmap.get("totalVarianceSmoke"));
            double standardDeviationLpg = Math.sqrt(varianceHashmap.get("totalVarianceLpg"));
            double standardDeviationOzone = Math.sqrt(varianceHashmap.get("totalVarianceOzone"));

            standardDeviationHashmap.put("standardDeviationCo", standardDeviationCo);
            standardDeviationHashmap.put("standardDeviationParticle", standardDeviationParticle);
            standardDeviationHashmap.put("standardDeviationSmoke", standardDeviationSmoke);
            standardDeviationHashmap.put("standardDeviationLpg", standardDeviationLpg);
            standardDeviationHashmap.put("standardDeviationOzone", standardDeviationOzone);

            standardDeviationList.add(standardDeviationHashmap);
        }

        return standardDeviationList;
    }

    // ---------------------- End of Calculate Standard Deviation --------------------------------

    // ---------------------- Calc the threshold values for each symptom ---------------------------

    public ArrayList<HashMap<String, Double>> calcThresholdValues() {
        ArrayList<HashMap<String, Double>> thresholdValueList = new ArrayList<HashMap<String, Double>>();
        for (int i = 0; i < symptomControllersList.size(); i++) {
            SymptomController symptomController = new SymptomController();
            symptomController = symptomControllersList.get(i);

            double standardDeviationCo = standardDeviationList.get(i).get("standardDeviationCo");
            double standardDeviationParticle = standardDeviationList.get(i).get("standardDeviationParticle");
            double standardDeviationLpg = standardDeviationList.get(i).get("standardDeviationSmoke");
            double standardDeviationSmoke = standardDeviationList.get(i).get("standardDeviationLpg");
            double standardDeviationOzone = standardDeviationList.get(i).get("standardDeviationOzone");

            double meanValueCo = hashmapMeansArrayList.get(i).get("coMean");
            double meanValueParticle = hashmapMeansArrayList.get(i).get("particleMean");
            double meanValueLpg = hashmapMeansArrayList.get(i).get("lpgMean");
            double meanValueSmoke = hashmapMeansArrayList.get(i).get("smokeMean");
            double meanValueOzone = hashmapMeansArrayList.get(i).get("ozoneMean");


            int validCountCo = 0;
            int validCountParticle = 0;
            int validCountLpg = 0;
            int validCountSmoke = 0;
            int validCountOzone = 0;

            for (int k = 0; k < symptomController.getAirReadingControllersList().size(); k++) {

                double originalValueCo = symptomController.getAirReadingControllersList().get(k).getCo();
                double originalValueParticle = symptomController.getAirReadingControllersList().get(k).getParticle();
                double originalValueLpg = symptomController.getAirReadingControllersList().get(k).getLpg();
                double originalValueSmoke = symptomController.getAirReadingControllersList().get(k).getSmoke();
                double originalValueOzone = symptomController.getAirReadingControllersList().get(k).getOzone();

                double checkingPlusValueCo = meanValueCo + standardDeviationCo;
                double checkingMinusValueCo = meanValueCo - standardDeviationCo;
                if (originalValueCo >= checkingMinusValueCo && originalValueCo <= checkingPlusValueCo) {
                    validCountCo++;
                }

                double checkingPlusValueParticle = meanValueParticle + standardDeviationParticle;
                double checkingMinusValueParticle = meanValueParticle - standardDeviationParticle;
                if (originalValueParticle >= checkingMinusValueParticle && originalValueParticle <= checkingPlusValueParticle) {
                    validCountParticle++;
                }

                double checkingPlusValueLpg = meanValueLpg + standardDeviationLpg;
                double checkingMinusValueLpg = meanValueLpg - standardDeviationLpg;
                if (originalValueLpg >= checkingMinusValueLpg && originalValueLpg <= checkingPlusValueLpg) {
                    validCountLpg++;
                }

                double checkingPlusValueSmoke = meanValueSmoke + standardDeviationSmoke;
                double checkingMinusValueSmoke = meanValueSmoke - standardDeviationSmoke;
                if (originalValueSmoke >= checkingMinusValueSmoke && originalValueSmoke <= checkingPlusValueSmoke) {
                    validCountSmoke++;
                }

                double checkingPlusValueOzone = meanValueOzone + standardDeviationOzone;
                double checkingMinusValueOzone = meanValueOzone - standardDeviationOzone;
                if (originalValueOzone >= checkingMinusValueOzone && originalValueOzone <= checkingPlusValueOzone) {
                    validCountOzone++;
                }
            }

            HashMap<String, Double> thresholdValueHashmap = new HashMap<String, Double>();
            double defaultvalue = -100;
            int noOfRecords = symptomController.getAirReadingControllersList().size();

            if (validCountCo > (noOfRecords / 2)) {
                thresholdValueHashmap.put("coThresholdValue", meanValueCo);
            } else {
                thresholdValueHashmap.put("coThresholdValue", defaultvalue);
            }

            if (validCountParticle > (noOfRecords / 2)) {
                thresholdValueHashmap.put("particleThresholdValue", meanValueParticle);
            } else {
                thresholdValueHashmap.put("particleThresholdValue", defaultvalue);
            }

            if (validCountLpg > (noOfRecords / 2)) {
                thresholdValueHashmap.put("lpgThresholdValue", meanValueLpg);
            } else {
                thresholdValueHashmap.put("lpgThresholdValue", defaultvalue);
            }

            if (validCountSmoke > (noOfRecords / 2)) {
                thresholdValueHashmap.put("smokeThresholdValue", meanValueSmoke);
            } else {
                thresholdValueHashmap.put("smokeThresholdValue", defaultvalue);
            }

            if (validCountOzone > (noOfRecords / 2)) {
                thresholdValueHashmap.put("ozoneThresholdValue", meanValueOzone);
            } else {
                thresholdValueHashmap.put("ozoneThresholdValue", defaultvalue);
            }

            thresholdValueList.add(thresholdValueHashmap);
        }
        return thresholdValueList;
    }

    // ----------------------End of Calc the threshold values for each symptom ---------------------------

}


