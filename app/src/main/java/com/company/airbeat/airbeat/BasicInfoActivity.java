package com.company.airbeat.airbeat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.company.airbeat.airbeat.Controller.AirReadingController;
import com.company.airbeat.airbeat.Controller.TriggerSymptomsController;
import com.company.airbeat.airbeat.Utils.TriggerAlgorithm;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.ArrayList;

public class BasicInfoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //----------------------------Navigation Drawer items--------------------------------
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView userNameView;
    private TextView userEmailView ;
    private MenuItem nav_logout_menu;
    private MenuItem nav_basic_info_menu;
    private MenuItem nav_bpm_menu;
    private MenuItem nav_symptoms_menu;
    private MenuItem nav_charts_menu;
    private MenuItem nav_home_menu;
    private MenuItem nav_trigger_menu;
    private MenuItem nav_set_alarm_menu;
    public static final String MyPREFERENCES = "MyPrefs" ;


    //---------------------------- Handlers ------------------------------------------
    Handler setNavigationHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            setNavigationViewListner();
        }
    };
    Handler setSharedPreferenceValuesHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            setSharedPreferenceValues();
        }
    };
    //----------------------------End of Handlers ------------------------------------
    //----------------------------End of navigation items----------------------------

    Handler getAirReadingsHandler = new Handler() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void handleMessage(Message msg) {
            getAirReadings();
        }
    };

    TextView coValueAirReading, particleValueAirReading, ozoneValueAirReading, lpgValueAirReading, smokeValueAirReading;

    TextView asthmaWarningText, asthmaSeverityText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_info);

        //----------------------------Navigation components----------------------------
        setNavigationHandler.sendEmptyMessage(0);

        drawerLayout = (DrawerLayout) findViewById(R.id.basic_info_page);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav=navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_basic_info_menu= menuNav.findItem(R.id.basic_info_menu);
        nav_bpm_menu= menuNav.findItem(R.id.bpm_menu);
        nav_symptoms_menu= menuNav.findItem(R.id.symptoms_menu);
        nav_charts_menu= menuNav.findItem(R.id.charts_menu);
        nav_trigger_menu= menuNav.findItem(R.id.trigger_menu);
        nav_home_menu= menuNav.findItem(R.id.home_menu);
        nav_set_alarm_menu = menuNav.findItem(R.id.set_alarm_menu);


        setSharedPreferenceValuesHandler.sendEmptyMessage(0);

        //----------------------------End of navigation components----------------------------

        //Calling the get air readings method
        Runnable getAirReadingsRunnable = new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        getAirReadingsHandler.sendEmptyMessage(0);
                    } catch (Exception ex) {
                        System.out.println("Error in getAirReadingsRunnable : " + ex);
                    }
                }
            }
        };

        coValueAirReading = (TextView)findViewById(R.id.coValueAirReadingText);
        particleValueAirReading = (TextView)findViewById(R.id.particleValueAirReadingText);
        ozoneValueAirReading = (TextView)findViewById(R.id.ozoneValueAirReadingText);
        lpgValueAirReading = (TextView)findViewById(R.id.lpgValueAirReadingText);
        smokeValueAirReading = (TextView)findViewById(R.id.smokeValueAirReadingText);
        asthmaWarningText = (TextView)findViewById(R.id.asthmaWarningText);
        asthmaSeverityText = (TextView)findViewById(R.id.asthmaSeverityText);

        //Creating the thread to be called the runnable task
        Thread getAirReadingsThread = new Thread(getAirReadingsRunnable);
        getAirReadingsThread.start();

    }

    //----------------------------Navigation methods----------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";

        email = sharedPreferences.getString("email","");
        fullname = sharedPreferences.getString("full_name","");


        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                homeFunction();
                break;
            }

            case R.id.basic_info_menu: {
                //Calling the basic info function
                basicInfoFunction();
                break;
            }

            case R.id.bpm_menu: {
                //Calling the bpm function
                bpmFunction();
                break;
            }

            case R.id.symptoms_menu: {
                //Calling the symptoms function
                symptomsFunction();
                break;
            }

            case R.id.charts_menu:{
                //Calling the charts function
                chartsFunction();
                break;
            }

            case R.id.set_alarm_menu:{
                //Calling the charts function
                setAlarmFunction();
                break;
            }

            case R.id.trigger_menu:{
                //Calling the my trigger function
                triggersFunction();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAlarmFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BasicInfoActivity.this,SetAlarmActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the home function for the navigation
    public void homeFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BasicInfoActivity.this,HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bsic info function for the navigation
    public void basicInfoFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BasicInfoActivity.this,BasicInfoActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bpm function for the navigation
    public void bpmFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BasicInfoActivity.this,BpmActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the symptoms function for the navigation
    public void symptomsFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BasicInfoActivity.this,SymptomsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the charts function for the navigation
    public void chartsFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BasicInfoActivity.this,ChartsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the triggers function for the navigation
    public void triggersFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(BasicInfoActivity.this,TriggerActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }


    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void logout_function(){
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(BasicInfoActivity.this,LoginActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }
    //----------------------------End of navigation methods----------------------------

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void getAirReadings(){
        String currentDate = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //LocalDateTime nowDate = LocalDateTime.now();
        Calendar calendar = Calendar.getInstance();
        currentDate = simpleDateFormat.format(calendar.getTime());

        AirReadingController airReadingController = new AirReadingController();
        airReadingController.setCurrentDate(currentDate);
        airReadingController = airReadingController.getAirReadings();

        //Set the labels
        coValueAirReading.setText(airReadingController.getCo() + "");
        particleValueAirReading.setText(airReadingController.getParticle()+"");
        ozoneValueAirReading.setText(airReadingController.getOzone()+"");
        lpgValueAirReading.setText(airReadingController.getLpg()+"");
        smokeValueAirReading.setText(airReadingController.getSmoke()+"");


        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String email="";
        email = sharedPreferences.getString("email","");
        String displayWarningtext="";

        ArrayList<String> userSymptomsList = new ArrayList<>();
        TriggerSymptomsController triggerSymptomsController = new TriggerSymptomsController();
        triggerSymptomsController.setUserEmail(email);
        userSymptomsList = triggerSymptomsController.checkTheSmptomsOfTheUser();

        for(int i=0;i<=userSymptomsList.size()/2;i+=2){
            String symptom = this.getSymptomName(userSymptomsList.get(i));
            String sensor = userSymptomsList.get(i+1);

            String sensorStr ="\"" + sensor + "Level is Causing "+symptom+"\"\n\n";
            displayWarningtext+= sensorStr;
        }

        TriggerAlgorithm triggerAlgorithm = new TriggerAlgorithm();
        asthmaSeverityText.setText(triggerAlgorithm.getSeverity(email));


        asthmaWarningText.setText(displayWarningtext);

    }

    public String getSymptomName(String idStr){
        String symptom="";
        int idInt = Integer.parseInt(idStr);
        switch (idInt){
            case 1:
                symptom="Wheezing or whistling when breathing";
                break;
            case 2:
                symptom="Coughing";
                break;
            case 3:
                symptom="Swollen airways";
                break;
            case 4:
                symptom="Development of mucus in the airways";
                break;
            case 5:
                symptom="Chest tightness and pain";
                break;
        }
        return symptom;
    }

}
