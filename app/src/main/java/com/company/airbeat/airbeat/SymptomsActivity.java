package com.company.airbeat.airbeat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.company.airbeat.airbeat.Controller.SymptomsInfoController;
import com.company.airbeat.airbeat.Controller.UserInfoController;
import com.company.airbeat.airbeat.Controller.UserSymptomController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SymptomsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Button symptom_add_btn;
    private Button symptom_reset_btn;
    private CheckBox symptom_wheezing_check_box;
    private CheckBox symptom_coughing_check_box;
    private CheckBox symptom_swollen_airways_check_box;
    private CheckBox symptom_development_of_muscus_check_box;
    private CheckBox symptom_chest_tightness_check_box;
    //----------------------------Navigation Drawer items--------------------------------
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private TextView userNameView;
    private TextView userEmailView;
    private MenuItem nav_logout_menu;
    private MenuItem nav_basic_info_menu;
    private MenuItem nav_bpm_menu;
    private MenuItem nav_symptoms_menu;
    private MenuItem nav_charts_menu;
    private MenuItem nav_home_menu;
    private MenuItem nav_trigger_menu;
    private MenuItem nav_set_alarm_menu;
    public static final String MyPREFERENCES = "MyPrefs";


    //---------------------------- Handlers ------------------------------------------
    Handler setNavigationHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setNavigationViewListner();
        }
    };
    Handler setSharedPreferenceValuesHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setSharedPreferenceValues();
        }
    };
    //----------------------------End of Handlers ------------------------------------
    //----------------------------End of navigation items----------------------------

    //Declaring and creating the handler method
    Handler loginHandler = new Handler() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void handleMessage(Message msg) {
            add_symptom_btn_function();
        }
    };
    Handler resetHandler = new Handler() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void handleMessage(Message msg) {
            reset_symptom_btn_function();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptoms);

        //----------------------------Navigation components----------------------------
        setNavigationHandler.sendEmptyMessage(0);

        drawerLayout = (DrawerLayout) findViewById(R.id.symptoms_page);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        userEmailView = (TextView) headerView.findViewById(R.id.userEmailID);
        userNameView = (TextView) headerView.findViewById(R.id.userNameID);

        Menu menuNav = navigationView.getMenu();
        nav_logout_menu = menuNav.findItem(R.id.logout_menu);
        nav_basic_info_menu = menuNav.findItem(R.id.basic_info_menu);
        nav_bpm_menu = menuNav.findItem(R.id.bpm_menu);
        nav_symptoms_menu = menuNav.findItem(R.id.symptoms_menu);
        nav_charts_menu = menuNav.findItem(R.id.charts_menu);
        nav_trigger_menu = menuNav.findItem(R.id.trigger_menu);
        nav_home_menu = menuNav.findItem(R.id.home_menu);
        nav_set_alarm_menu = menuNav.findItem(R.id.set_alarm_menu);


        setSharedPreferenceValuesHandler.sendEmptyMessage(0);

        //----------------------------End of navigation components----------------------------

        symptom_wheezing_check_box = (CheckBox) findViewById(R.id.symptom_wheezing_id);
        symptom_coughing_check_box = (CheckBox) findViewById(R.id.symptom_coughing_id);
        symptom_swollen_airways_check_box = (CheckBox) findViewById(R.id.symptom_swollen_airways_id);
        symptom_development_of_muscus_check_box = (CheckBox) findViewById(R.id.symptom_development_of_muscus_id);
        symptom_chest_tightness_check_box = (CheckBox) findViewById(R.id.symptom_chest_tightness_id);

        symptom_add_btn = (Button) findViewById(R.id.symptom_add_button);
        symptom_reset_btn = (Button) findViewById(R.id.symptom_reset_button);

        symptom_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating the runnable method
                Runnable loginRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                loginHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in add click on symptom page : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread loginThread = new Thread(loginRunnable);
                loginThread.start();
            }
        });

        symptom_reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating the runnable method
                Runnable loginRunnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                resetHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in reseting in symptoms page : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread loginThread = new Thread(loginRunnable);
                loginThread.start();
            }
        });
    }

    //----------------------------Navigation methods----------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Function to set the values retreiving from the shared preference
    public void setSharedPreferenceValues() {
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Declaring variables
        String fullname="";
        String email="";

        email = sharedPreferences.getString("email","");
        fullname = sharedPreferences.getString("full_name","");


        //Setting the values
        userEmailView.setText(email);
        userNameView.setText(fullname);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_menu: {
                //Calling the logout function
                logout_function();
                break;
            }

            case R.id.home_menu: {
                //Calling the home function
                homeFunction();
                break;
            }

            case R.id.basic_info_menu: {
                //Calling the basic info function
                basicInfoFunction();
                break;
            }

            case R.id.bpm_menu: {
                //Calling the bpm function
                bpmFunction();
                break;
            }

            case R.id.symptoms_menu: {
                //Calling the symptoms function
                symptomsFunction();
                break;
            }

            case R.id.charts_menu: {
                //Calling the charts function
                chartsFunction();
                break;
            }

            case R.id.set_alarm_menu:{
                //Calling the charts function
                setAlarmFunction();
                break;
            }

            case R.id.trigger_menu: {
                //Calling the my trigger function
                triggersFunction();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAlarmFunction(){
        //Setting the intent to redirect the page
        Intent intent = new Intent(SymptomsActivity.this,SetAlarmActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the home function for the navigation
    public void homeFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SymptomsActivity.this, HomeActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bsic info function for the navigation
    public void basicInfoFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SymptomsActivity.this, BasicInfoActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the bpm function for the navigation
    public void bpmFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SymptomsActivity.this, BpmActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the symptoms function for the navigation
    public void symptomsFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SymptomsActivity.this, SymptomsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the charts function for the navigation
    public void chartsFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SymptomsActivity.this, ChartsActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //the triggers function for the navigation
    public void triggersFunction() {
        //Setting the intent to redirect the page
        Intent intent = new Intent(SymptomsActivity.this, TriggerActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }


    private void setNavigationViewListner() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void logout_function() {
        //Creating the Shared Preference object
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //Removing all the values in the shared preference
        sharedPreferences.edit().clear().commit();
        //Setting the intent to redirect the page
        Intent intent = new Intent(SymptomsActivity.this, LoginActivity.class);
        //Redirecting the page
        startActivity(intent);
        finish();
    }

    //----------------------------End of navigation methods----------------------------
    //--------------- Normal methods ------------------------------------
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void add_symptom_btn_function() {

        ArrayList<String> symptoms_array = new ArrayList<>();

        //Getting the checked values
        if (symptom_wheezing_check_box.isChecked()) {
            symptoms_array.add("WW");
        }

        if (symptom_coughing_check_box.isChecked()) {
            symptoms_array.add("C");
        }

        if (symptom_swollen_airways_check_box.isChecked()) {
            symptoms_array.add("SA");
        }

        if (symptom_development_of_muscus_check_box.isChecked()) {
            symptoms_array.add("DM");
        }

        if (symptom_chest_tightness_check_box.isChecked()) {
            symptoms_array.add("CT");
        }

        if (symptom_wheezing_check_box.isChecked() != true && symptom_coughing_check_box.isChecked() != true &&
                symptom_swollen_airways_check_box.isChecked() != true && symptom_development_of_muscus_check_box.isChecked() != true &&
                symptom_chest_tightness_check_box.isChecked() != true) {
            Toast.makeText(this, "You have not selected any symptoms", Toast.LENGTH_LONG).show();
        } else {
            //Getting the current date in 'YYYY-MM-DD' format
            String currentDate = getCurrentDate();

            //Getting the current time in 'HH:MM:SS' format
            String currentTime = getCurrentTime();

            //Populate the symptoms array
            List<SymptomsInfoController> symptomsInfoControllerList = new ArrayList<>();

            for(int i=0;i<symptoms_array.size();i++){
                SymptomsInfoController symptomsInfoController = new SymptomsInfoController();
                symptomsInfoController.setSymptomIdentifier(symptoms_array.get(i));
                symptomsInfoControllerList.add(symptomsInfoController);
            }

            //Getting the user email
            //Creating the Shared Preference object
            SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String email = sharedPreferences.getString("email","");

            UserInfoController userInfoController = new UserInfoController();
            userInfoController.setEmail(email);

            UserSymptomController userSymptomController = new UserSymptomController(userInfoController);
            userSymptomController.setSymptomsInfoControllerList(symptomsInfoControllerList);
            userSymptomController.setCurrentDate(currentDate);
            userSymptomController.setCurrentTime(currentTime);

            userSymptomController.insertSelectedSymptoms();


            Toast.makeText(this, "You have successfully inserted the Symptoms", Toast.LENGTH_LONG).show();
            //Setting the intent to redirect the page
            Intent intent = new Intent(SymptomsActivity.this, SymptomsActivity.class);
            //Redirecting the page
            startActivity(intent);
            finish();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getCurrentDate() {
        String currentDate = "";
        DateTimeFormatter dtfDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime nowDate = LocalDateTime.now();
        currentDate = dtfDate.format(nowDate);
        return currentDate;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getCurrentTime() {
        String currentTime = "";
        DateTimeFormatter dtfTime = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime nowTime = LocalDateTime.now();
        currentTime = dtfTime.format(nowTime);
        return currentTime;
    }

    public void reset_symptom_btn_function() {
        symptom_wheezing_check_box.setChecked(false);
        symptom_coughing_check_box.setChecked(false);
        symptom_swollen_airways_check_box.setChecked(false);
        symptom_development_of_muscus_check_box.setChecked(false);
        symptom_chest_tightness_check_box.setChecked(false);
        Toast.makeText(this, "All the symptom checkbox's are resetted", Toast.LENGTH_LONG).show();
    }
}
