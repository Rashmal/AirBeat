package com.company.airbeat.airbeat.Controller;

import android.os.AsyncTask;

public class LoginUser extends AsyncTask<String,String,Boolean> {
    //Attributes
    private UserInfoController userInfoController;
    private boolean loginStatus;

    //Constructor
    public LoginUser(UserInfoController userInfoController){
        this.userInfoController = userInfoController;
    }

    //Getter
    public boolean isLoginStatus() {
        return loginStatus;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        this.loginStatus = this.userInfoController.loginUser();
        return  this.loginStatus;
    }

    @Override
    protected void onPostExecute(Boolean s) {

    }
}
