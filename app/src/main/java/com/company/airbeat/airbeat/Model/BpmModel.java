package com.company.airbeat.airbeat.Model;

import com.company.airbeat.airbeat.Controller.AirReadingController;
import com.company.airbeat.airbeat.Controller.BpmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


import static com.company.airbeat.airbeat.Utils.ScriptPaths.GET_BPM_READINGS_FOR_DURATIONS;

public class BpmModel {

    public ArrayList<BpmController> getBpmReadingsForDuration(final BpmController bpmController){
        final String[] resultString = new String[1];
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    URL url = new URL(GET_BPM_READINGS_FOR_DURATIONS);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(bpmController.getCurrentDate(), "UTF-8")
                            + "&" +
                            URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(bpmController.getUserInfoController().getEmail(), "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    //return result;
                    resultString[0] = result;


                } catch (Exception e) {
                    System.out.println("Error in getAirReadingsForDuration in BpmModel Class : " + e);
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        JSONArray obj = null;
        try {
            obj = new JSONArray(resultString[0]);


        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<BpmController> bpmControllerList = new ArrayList<>();

        //Iterating the jason array
        int jsonArraySize = obj.length();
        String kk="";

        for(int i=0;i<jsonArraySize;i++){
            BpmController airReadingController = new BpmController();
            JSONObject jsonObject=null;
            try {
                jsonObject = new JSONObject(obj.get(i).toString());
                airReadingController.setBpmValue(jsonObject.getDouble("bpmValue"));
                airReadingController.setCurrentTime(jsonObject.getString("CurrentTime"));

                bpmControllerList.add(airReadingController);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return bpmControllerList;
    }
}
