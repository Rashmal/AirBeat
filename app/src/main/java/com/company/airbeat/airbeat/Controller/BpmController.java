package com.company.airbeat.airbeat.Controller;

import com.company.airbeat.airbeat.Model.BpmModel;

import java.util.ArrayList;

public class BpmController {
    private int BpmID;
    private double bpmValue;
    private String currentDate;
    private String currentTime;
    private UserInfoController userInfoController = new UserInfoController();


    public UserInfoController getUserInfoController() {
        return userInfoController;
    }

    public void setUserInfoController(UserInfoController userInfoController) {
        this.userInfoController = userInfoController;
    }

    public int getBpmID() {
        return BpmID;
    }

    public void setBpmID(int bpmID) {
        BpmID = bpmID;
    }

    public double getBpmValue() {
        return bpmValue;
    }

    public void setBpmValue(double bpmValue) {
        this.bpmValue = bpmValue;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }


    public ArrayList<BpmController> getBpmReadingsForDuration(){
        BpmModel bpmModel = new BpmModel();
        return bpmModel.getBpmReadingsForDuration(this);
    }
}
