package com.company.airbeat.airbeat.Controller;

import com.company.airbeat.airbeat.Model.UserSymptomsModel;

import java.util.ArrayList;
import java.util.List;

public class UserSymptomController {
    //Attributes
    private int userSymptomID;
    private String currentDate;
    private String currentTime;
    private String email;
    private String previousDate;
    private UserInfoController userInfoController;
    private List<SymptomsInfoController> symptomsInfoControllerList = new ArrayList<>();
    private List<String> symptomIdentifiers = new ArrayList<>();

    //Constructor

    public UserSymptomController(UserInfoController userInfoController) {
        this.userInfoController = userInfoController;
    }


    //Setters

    public void setUserSymptomID(int userSymptomID) {
        this.userSymptomID = userSymptomID;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPreviousDate(String previousDate) {
        this.previousDate = previousDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public void setUserInfoController(UserInfoController userInfoController) {
        this.userInfoController = userInfoController;
    }

    public void setSymptomsInfoControllerList(List<SymptomsInfoController> symptomsInfoController) {
        this.symptomsInfoControllerList = symptomsInfoController;
        populateSymptomsList();
    }

    //Getters

    public int getUserSymptomID() {
        return userSymptomID;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public String getEmail() {
        return email;
    }

    public String getPreviousDate() {
        return previousDate;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public UserInfoController getUserInfoController() {
        return userInfoController;
    }

    public List<SymptomsInfoController> getSymptomsInfoControllerList() {
        return symptomsInfoControllerList;
    }

    public List<String> getSymptomIdentifiers() {
        return symptomIdentifiers;
    }

    //Methods
    public void insertSelectedSymptoms(){
        UserSymptomsModel userSymptomsModel = new UserSymptomsModel();
        userSymptomsModel.insertSelectedSymptoms(this);
    }

    public void populateSymptomsList(){
        for(int i=0;i<this.getSymptomsInfoControllerList().size();i++){
            this.symptomIdentifiers.add(this.getSymptomsInfoControllerList().get(i).getSymptomIdentifier());
        }
    }


    public ArrayList<String> getSymptomsTableValues(){
        UserSymptomsModel userSymptomsModel = new UserSymptomsModel();
        return userSymptomsModel.getSymptomsTableValues(this);
    }

    public int getCount(){
        UserSymptomsModel userSymptomsModel = new UserSymptomsModel();
        return userSymptomsModel.getCount(this);
    }



}
