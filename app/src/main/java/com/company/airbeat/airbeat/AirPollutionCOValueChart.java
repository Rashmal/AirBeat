package com.company.airbeat.airbeat;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.company.airbeat.airbeat.Controller.AirReadingController;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AirPollutionCOValueChart extends AppCompatActivity {

    private static final String TAG = "AirPollutionChart";
    private LineChart coLineChart;
    private Button setStartDateButton, refreshChartBUtton;
    private TextView startDateTextView;
    static final int DIALOG_ID = 0;
    int year_x,month_x,day_x;

    Handler coChartHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            refreshChart();
        }
    };

    Handler setStartDateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            showDialog(DIALOG_ID);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_pollution_covalue_chart);

        coLineChart = (LineChart) findViewById(R.id.air_pollution_co_line_chart_id);

        setStartDateButton = (Button) findViewById(R.id.setStartDateBtn);
        refreshChartBUtton = (Button) findViewById(R.id.refreshChartBtn);
        startDateTextView = (TextView) findViewById(R.id.startDateText);

        final Calendar calendar = Calendar.getInstance();
        year_x = calendar.get(Calendar.YEAR);
        month_x = calendar.get(Calendar.MONTH);
        day_x = calendar.get(Calendar.DAY_OF_MONTH);

        setStartDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                setStartDateHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in setStartDateButton : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(runnable);
                thread.start();

            }
        });

        refreshChartBUtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                coChartHandler.sendEmptyMessage(0);
                            } catch (Exception ex) {
                                System.out.println("Error in setEndDateHandler : " + ex);
                            }
                        }
                    }
                };

                //Creating the thread to be called the runnable task
                Thread thread = new Thread(runnable);
                thread.start();
            }
        });


    }


    public void refreshChart(){

        ArrayList<AirReadingController> airReadingControllerList = new ArrayList<>();

        AirReadingController airReadingController = new AirReadingController();
        airReadingController.setStartDate(startDateTextView.getText().toString());
        airReadingControllerList = airReadingController.getAirReadingsForDuration();

        if(startDateTextView.getText().toString().equals("yyyy-mm-dd")){
            Toast.makeText(this, "Error: Please select the dates", Toast.LENGTH_LONG).show();
        }else{
            if(airReadingControllerList.size()>0) {
                setCOValueChart(airReadingControllerList);
            }else{
                Toast.makeText(this, "Error: Not enough data to show on the chart", Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    protected Dialog onCreateDialog(int id){
        if(id == DIALOG_ID) {
            return new DatePickerDialog(this, dpickerListner, year_x, month_x, day_x);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener dpickerListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            year_x=i;
            month_x=i1+1;
            day_x=i2;

            String monthStr=month_x+"";
            String dayStr=day_x+"";

            if(month_x<10){
                monthStr = "0"+month_x;
            }
            if(day_x<10){
                dayStr = "0"+day_x;
            }

            startDateTextView.setText(year_x+"-"+monthStr+"-"+dayStr);
        }
    };

    public void setCOValueChart(ArrayList<AirReadingController> airReadingControllerList) {

        coLineChart.clear();

        coLineChart.setDragEnabled(true);
        coLineChart.setScaleEnabled(false);

        LimitLine upper_limit = new LimitLine(65f, "Danger");
        upper_limit.setLineWidth(4f);
        upper_limit.enableDashedLine(10f, 10f, 0f);
        upper_limit.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        upper_limit.setTextSize(15f);

        YAxis lextAxis = coLineChart.getAxisLeft();
        lextAxis.removeAllLimitLines();
        lextAxis.addLimitLine(upper_limit);
        lextAxis.enableGridDashedLine(10f, 10f, 0);
        lextAxis.setDrawLimitLinesBehindData(true);

        coLineChart.getAxisRight().setEnabled(false);

        ArrayList<Entry> yValues = new ArrayList<>();

        for(int i=0;i<airReadingControllerList.size();i++){
            float valueFloat = (float) airReadingControllerList.get(i).getCo();
            yValues.add(new Entry((i+1), valueFloat));
        }


        LineDataSet set1 = new LineDataSet(yValues, "Data set 1");

        set1.setFillAlpha(110);
        set1.setColor(Color.RED);
        set1.setLineWidth(3f);
        set1.setValueTextSize(10f);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        //Converting the array list to string array
        String[] values = new String[airReadingControllerList.size()];
        for(int i=0;i<airReadingControllerList.size();i++){
            values[i]=airReadingControllerList.get(i).getCurrentTime();
        }

        MyXAxisValueFormatter myXAxisValueFormatter = new MyXAxisValueFormatter();
        myXAxisValueFormatter.setmValues(values);

        LineData data = new LineData(dataSets);

        coLineChart.setData(data);





            XAxis xAxis = coLineChart.getXAxis();
            xAxis.setValueFormatter(myXAxisValueFormatter);
            xAxis.setGranularity(1f);


        data.notifyDataChanged();
        coLineChart.notifyDataSetChanged();
        coLineChart.invalidate();
    }

    //X axis value formatter class
    public class MyXAxisValueFormatter implements IAxisValueFormatter {
        private String[] mValues;

        public String[] getmValues() {
            return mValues;
        }

        public void setmValues(String[] mValues) {
            this.mValues = mValues;
        }

        public MyXAxisValueFormatter() {

        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return mValues[((int) value) - 1];
        }
    }
}
