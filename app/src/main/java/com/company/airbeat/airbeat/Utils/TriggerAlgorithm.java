package com.company.airbeat.airbeat.Utils;

import android.content.Intent;

import com.company.airbeat.airbeat.Controller.AirReadingController;
import com.company.airbeat.airbeat.Controller.TriggerSymptomsController;
import com.company.airbeat.airbeat.Controller.UserInfoController;
import com.company.airbeat.airbeat.Controller.UserSymptomController;
import com.company.airbeat.airbeat.Model.AirReadingModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TriggerAlgorithm {

       private double particle, co, lpg=11, smoke, cdate, ctime;
       private double thparticle, thco, thlpg, thsmoke, thcdate, thctime;
       private String type_of_asthma;

       private AirReadingController airReadingController;

       private UserInfoController userInfoController = new UserInfoController();

       private UserSymptomController symptomController = new UserSymptomController(userInfoController);


    public String getSeverityBasedOnDuration(String email,String startDate, String endDate){

        int noTimes = getOccurenceBasedOnDuration(email,startDate,endDate);
        if (noTimes <=2 && noTimes !=0 ){
            type_of_asthma = "Mild Intermittent";
        }
        else if (noTimes >= 3 && noTimes <= 6){
            type_of_asthma = "Mild Persistent";
        }
        else if (noTimes < 8 && noTimes >=7){
            type_of_asthma = "Moderate Persistent";
        }
        else{
            type_of_asthma = "Severe Persistent";
        }

        return type_of_asthma;


    }

    public int getOccurenceBasedOnDuration(String email,String startDate, String endDate){

        UserInfoController userInfoController = new UserInfoController();
        userInfoController.setEmail(email);

        UserSymptomController userSymptomController = new UserSymptomController(userInfoController);
        userSymptomController.setCurrentDate(endDate);
        userSymptomController.setPreviousDate(startDate);
        userSymptomController.setEmail(email);


        return userSymptomController.getCount();
    }



       public String getSeverity(String email){

           int noTimes = getOccurence(email);
           if (noTimes <=2 && noTimes !=0 ){
               type_of_asthma = "Mild Intermittent";
           }
           else if (noTimes >= 3 && noTimes <= 6){
               type_of_asthma = "Mild Persistent";
           }
           else if (noTimes < 8 && noTimes >=7){
               type_of_asthma = "Moderate Persistent";
           }
           else{
               type_of_asthma = "Severe Persistent";
           }

           return type_of_asthma;


       }

       public String getCurrentDate(){
           Calendar calendar = Calendar.getInstance();
           SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
           String strDate = simpleDateFormat.format(calendar.getTime());
           return strDate;
       }

       public String getPreviousDate(){

           Calendar calendar = Calendar.getInstance();
           calendar.setTime(calendar.getTime());
           calendar.add(Calendar.DAY_OF_YEAR, -7);
           SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
           String strDate = simpleDateFormat.format(calendar.getTime());
           return strDate;
       }

       public int getOccurence(String email){

           UserInfoController userInfoController = new UserInfoController();
           userInfoController.setEmail(email);

           UserSymptomController userSymptomController = new UserSymptomController(userInfoController);
           userSymptomController.setCurrentDate(getCurrentDate());
           userSymptomController.setPreviousDate(getPreviousDate());
           userSymptomController.setEmail(email);


           return userSymptomController.getCount();
       }

       public String symptomIdentification(String email){
           String result = "";


           ArrayList<String> userSymptomsList = new ArrayList<>();
           TriggerSymptomsController triggerSymptomsController = new TriggerSymptomsController();
           triggerSymptomsController.setUserEmail(email);
           userSymptomsList = triggerSymptomsController.checkTheSmptomsOfTheUser();

           for(int i=0;i<=userSymptomsList.size()/2;i+=2){
               String symptom = this.getSymptomName(userSymptomsList.get(i));
               String sensor = userSymptomsList.get(i+1);

               String sensorStr ="\"" + sensor + "Level is Causing "+symptom+"\"\n\n";
               result+= sensorStr;
           }

           return result;
       }

       public int getThreshold(){
           return 11;
       }

       public AirReadingModel getLiveData(){

           return null;
       }

       public String getSymptomName(String idStr){
           String symptom="";
           int idInt = Integer.parseInt(idStr);
           switch (idInt){
               case 1:
                   symptom="Wheezing or whistling when breathing";
                   break;
               case 2:
                   symptom="Coughing";
                   break;
               case 3:
                   symptom="Swollen airways";
                   break;
               case 4:
                   symptom="Development of mucus in the airways";
                   break;
               case 5:
                   symptom="Chest tightness and pain";
                   break;
           }
           return symptom;
       }


}
