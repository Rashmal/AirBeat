-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2018 at 10:02 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airbeat`
--

-- --------------------------------------------------------

--
-- Table structure for table `airreading`
--

CREATE TABLE `airreading` (
  `Particle` double NOT NULL,
  `Methane` double NOT NULL,
  `Co` double NOT NULL,
  `LPG` double NOT NULL,
  `Smoke` double NOT NULL,
  `ID` int(11) NOT NULL,
  `CurrentDate` varchar(500) NOT NULL,
  `CurrentTime` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `airreading`
--

INSERT INTO `airreading` (`Particle`, `Methane`, `Co`, `LPG`, `Smoke`, `ID`, `CurrentDate`, `CurrentTime`) VALUES
(12, 11, 14, 11, 17, 1, '2018-06-18', '15:30:00'),
(16, 15, 33, 12, 22, 2, '2018-06-18', '15:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `bpm`
--

CREATE TABLE `bpm` (
  `BpmID` int(11) NOT NULL,
  `bpmValue` double NOT NULL,
  `UserID` int(11) NOT NULL,
  `CurrentDate` varchar(255) NOT NULL,
  `CurrentTime` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `symptomsinfo`
--

CREATE TABLE `symptomsinfo` (
  `SymptomID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `SymptomIdentifier` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `symptomsinfo`
--

INSERT INTO `symptomsinfo` (`SymptomID`, `Name`, `SymptomIdentifier`) VALUES
(1, 'Wheezing or whistling when breathing', 'WW'),
(2, 'Coughing', 'C'),
(3, 'Swollen airways', 'SA'),
(4, 'Development of mucus in the airways', 'DM'),
(5, 'Chest tightness and pain', 'CT');

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `UserID` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `UserPassword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`UserID`, `FirstName`, `LastName`, `Email`, `UserPassword`) VALUES
(7, 'Trisha', 'Prins', 'trisha@gmail.com', 'trisha'),
(5, 'Rashmal', 'Perera', 'rashmalat@gmail.com', 'rashmal'),
(6, 'AA', 'aa', 'aa@gmail.com', 'aa'),
(16, 'kk', 'kk', 'kk@gmail.com', 'kk');

-- --------------------------------------------------------

--
-- Table structure for table `usersymptoms`
--

CREATE TABLE `usersymptoms` (
  `UserSymptomID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `SymptomID` int(11) NOT NULL,
  `CurrentDate` varchar(255) NOT NULL,
  `CurrentTime` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usersymptoms`
--

INSERT INTO `usersymptoms` (`UserSymptomID`, `UserID`, `SymptomID`, `CurrentDate`, `CurrentTime`) VALUES
(7, 5, 4, '2018-06-18', '15:02:36'),
(6, 5, 1, '2018-06-18', '15:02:36'),
(8, 5, 1, '2018-06-18', '15:02:50'),
(9, 5, 3, '2018-06-18', '15:02:50'),
(10, 5, 5, '2018-06-18', '15:02:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airreading`
--
ALTER TABLE `airreading`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `bpm`
--
ALTER TABLE `bpm`
  ADD PRIMARY KEY (`BpmID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `symptomsinfo`
--
ALTER TABLE `symptomsinfo`
  ADD PRIMARY KEY (`SymptomID`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `usersymptoms`
--
ALTER TABLE `usersymptoms`
  ADD PRIMARY KEY (`UserSymptomID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `SymptomID` (`SymptomID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airreading`
--
ALTER TABLE `airreading`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bpm`
--
ALTER TABLE `bpm`
  MODIFY `BpmID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `symptomsinfo`
--
ALTER TABLE `symptomsinfo`
  MODIFY `SymptomID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `usersymptoms`
--
ALTER TABLE `usersymptoms`
  MODIFY `UserSymptomID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
