-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2018 at 06:21 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airbeat`
--

-- --------------------------------------------------------

--
-- Table structure for table `airreading`
--

CREATE TABLE `airreading` (
  `Particle` double NOT NULL,
  `Ozone` double NOT NULL,
  `Co` double NOT NULL,
  `LPG` double NOT NULL,
  `Smoke` double NOT NULL,
  `ID` int(11) NOT NULL,
  `CurrentDate` varchar(500) NOT NULL,
  `CurrentTime` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `airreading`
--

INSERT INTO `airreading` (`Particle`, `Ozone`, `Co`, `LPG`, `Smoke`, `ID`, `CurrentDate`, `CurrentTime`) VALUES
(12, 11, 14, 11, 17, 1, '2018-06-18', '15:02:30'),
(16, 15, 18, 12, 22, 2, '2018-06-18', '15:02:40'),
(16, 12, 14, 11, 20, 3, '2018-06-18', '15:02:50'),
(14, 14, 15, 10, 18, 4, '2018-06-18', '15:03:00'),
(12, 11, 18, 11, 20, 5, '2018-06-18', '15:03:10'),
(16, 11, 14, 12, 21, 6, '2018-06-18', '15:03:20'),
(14, 14, 16, 11, 22, 7, '2018-06-28', '15:03:30');

-- --------------------------------------------------------

--
-- Table structure for table `bpm`
--

CREATE TABLE `bpm` (
  `BpmID` int(11) NOT NULL,
  `bpmValue` double NOT NULL,
  `UserID` int(11) NOT NULL,
  `CurrentDate` varchar(255) NOT NULL,
  `CurrentTime` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `symptomsinfo`
--

CREATE TABLE `symptomsinfo` (
  `SymptomID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `SymptomIdentifier` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `symptomsinfo`
--

INSERT INTO `symptomsinfo` (`SymptomID`, `Name`, `SymptomIdentifier`) VALUES
(1, 'Wheezing or whistling when breathing', 'WW'),
(2, 'Coughing', 'C'),
(3, 'Swollen airways', 'SA'),
(4, 'Development of mucus in the airways', 'DM'),
(5, 'Chest tightness and pain', 'CT');

-- --------------------------------------------------------

--
-- Table structure for table `symptomtrigger`
--

CREATE TABLE `symptomtrigger` (
  `id` int(11) NOT NULL,
  `SymptomID` int(11) NOT NULL,
  `SymptomThreshold` double NOT NULL,
  `UserEmail` varchar(255) NOT NULL,
  `airreadingComponent` varchar(500) DEFAULT NULL,
  `airReadingID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `symptomtrigger`
--

INSERT INTO `symptomtrigger` (`id`, `SymptomID`, `SymptomThreshold`, `UserEmail`, `airreadingComponent`, `airReadingID`) VALUES
(133, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(132, 4, 11, 'rashmalat@gmail.com', 'Ozone', 7),
(131, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(130, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(129, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(128, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(127, 3, 12, 'rashmalat@gmail.com', 'Ozone', 7),
(124, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(126, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(125, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(123, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(121, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(122, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(120, 5, 12, 'rashmalat@gmail.com', 'Ozone', 7),
(117, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(118, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(119, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(114, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(115, 4, 11, 'rashmalat@gmail.com', 'Ozone', 7),
(116, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(113, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(112, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(110, 3, 12, 'rashmalat@gmail.com', 'Ozone', 7),
(111, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(108, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(109, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(107, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(106, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(105, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(104, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(134, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(135, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(136, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(137, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(138, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(139, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(140, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(141, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(142, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(143, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(144, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(145, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(146, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(147, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(148, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(149, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(150, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(151, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(152, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(153, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(154, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(155, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(156, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(157, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(158, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(159, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(160, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(161, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(162, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(163, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(164, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(165, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(166, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(167, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(168, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(169, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(170, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(171, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(172, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(173, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(174, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(175, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(176, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(177, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(178, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(179, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(180, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(181, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(182, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(183, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(184, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(185, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(186, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(187, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(188, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(189, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(190, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(191, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(192, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(193, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(194, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(195, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(196, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(197, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(198, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(199, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(200, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(201, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(202, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(203, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(204, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(205, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(206, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(207, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(208, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(209, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(210, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(211, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(212, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(213, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(214, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(215, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(216, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(217, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(218, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(219, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(220, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(221, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(222, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(223, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(224, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(225, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(226, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(227, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(228, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(229, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(230, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(231, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(232, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(233, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(234, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(235, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(236, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(237, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(238, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(239, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(240, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(241, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(242, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(243, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(244, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(245, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(246, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(247, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(248, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(249, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(250, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(251, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(252, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(253, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(254, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(255, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(256, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(257, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(258, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(259, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(260, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(261, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(262, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(263, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(264, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(265, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(266, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(267, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(268, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(269, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(270, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(271, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(272, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(273, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(274, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(275, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(276, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(277, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(278, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(279, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(280, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(281, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(282, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(283, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(284, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(285, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(286, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(287, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(288, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(289, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(290, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(291, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(292, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(293, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(294, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(295, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(296, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(297, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(298, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(299, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(300, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(301, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(302, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(303, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(304, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(305, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(306, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(307, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(308, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(309, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(310, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(311, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(312, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(313, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(314, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(315, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(316, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(317, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(318, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(319, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(320, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(321, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(322, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(323, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(324, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(325, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(326, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(327, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(328, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(329, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(330, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(331, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(332, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(333, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(334, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(335, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(336, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(337, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(338, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(339, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(340, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(341, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(342, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(343, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(344, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(345, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(346, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(347, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(348, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(349, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(350, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(351, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(352, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(353, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(354, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(355, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(356, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(357, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(358, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(359, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(360, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(361, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(362, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(363, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(364, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(365, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(366, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(367, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(368, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(369, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(370, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(371, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(372, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(373, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(374, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(375, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(376, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(377, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(378, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(379, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(380, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(381, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(382, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(383, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(384, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(385, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(386, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(387, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(388, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(389, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(390, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(391, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(392, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(393, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(394, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(395, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(396, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(397, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(398, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(399, 3, 12, 'rashmalat@gmail.com', 'Methane', 7),
(400, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(401, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(402, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(403, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(404, 4, 11, 'rashmalat@gmail.com', 'Methane', 7),
(405, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(406, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(407, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(408, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(409, 5, 12, 'rashmalat@gmail.com', 'Methane', 7),
(410, 1, 14.333333333333334, 'rashmalat@gmail.com', 'Co', 7),
(411, 1, 10.666666666666666, 'rashmalat@gmail.com', 'LPG', 7),
(412, 3, 14, 'rashmalat@gmail.com', 'Co', 7),
(413, 3, 16, 'rashmalat@gmail.com', 'Particle', 7),
(414, 3, 11, 'rashmalat@gmail.com', 'LPG', 7),
(415, 3, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(416, 3, 12, 'rashmalat@gmail.com', 'Ozone', 7),
(417, 4, 14, 'rashmalat@gmail.com', 'Co', 7),
(418, 4, 12, 'rashmalat@gmail.com', 'Particle', 7),
(419, 4, 11, 'rashmalat@gmail.com', 'LPG', 7),
(420, 4, 17, 'rashmalat@gmail.com', 'Smoke', 7),
(421, 4, 11, 'rashmalat@gmail.com', 'Ozone', 7),
(422, 5, 14, 'rashmalat@gmail.com', 'Co', 7),
(423, 5, 16, 'rashmalat@gmail.com', 'Particle', 7),
(424, 5, 11, 'rashmalat@gmail.com', 'LPG', 7),
(425, 5, 20, 'rashmalat@gmail.com', 'Smoke', 7),
(426, 5, 12, 'rashmalat@gmail.com', 'Ozone', 7);

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `UserID` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `UserPassword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`UserID`, `FirstName`, `LastName`, `Email`, `UserPassword`) VALUES
(5, 'Rashmal', 'Perera', 'rashmalat@gmail.com', 'rashmal'),
(6, 'AA', 'aa', 'aa@gmail.com', 'aa'),
(16, 'kk', 'kk', 'kk@gmail.com', 'kk'),
(17, 'test', 'test', 'style@test.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `usersymptoms`
--

CREATE TABLE `usersymptoms` (
  `UserSymptomID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `SymptomID` int(11) NOT NULL,
  `CurrentDate` varchar(255) NOT NULL,
  `CurrentTime` varchar(255) NOT NULL,
  `airreadingid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usersymptoms`
--

INSERT INTO `usersymptoms` (`UserSymptomID`, `UserID`, `SymptomID`, `CurrentDate`, `CurrentTime`, `airreadingid`) VALUES
(7, 5, 4, '2018-06-18', '15:02:36', 1),
(6, 5, 1, '2018-06-18', '15:02:36', 1),
(8, 5, 1, '2018-06-18', '15:02:50', 3),
(9, 5, 3, '2018-06-18', '15:02:50', 3),
(10, 5, 5, '2018-06-18', '15:02:50', 3),
(11, 5, 1, '2018-06-18', '15:03:00', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airreading`
--
ALTER TABLE `airreading`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `bpm`
--
ALTER TABLE `bpm`
  ADD PRIMARY KEY (`BpmID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `symptomsinfo`
--
ALTER TABLE `symptomsinfo`
  ADD PRIMARY KEY (`SymptomID`);

--
-- Indexes for table `symptomtrigger`
--
ALTER TABLE `symptomtrigger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `usersymptoms`
--
ALTER TABLE `usersymptoms`
  ADD PRIMARY KEY (`UserSymptomID`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `SymptomID` (`SymptomID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airreading`
--
ALTER TABLE `airreading`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bpm`
--
ALTER TABLE `bpm`
  MODIFY `BpmID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `symptomsinfo`
--
ALTER TABLE `symptomsinfo`
  MODIFY `SymptomID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `symptomtrigger`
--
ALTER TABLE `symptomtrigger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=427;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `usersymptoms`
--
ALTER TABLE `usersymptoms`
  MODIFY `UserSymptomID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
